package com.sskorupski.willshare;

import android.support.test.runner.AndroidJUnit4;

import com.sskorupski.willshare.features.main.servers.OnClickWillServers;
import com.sskorupski.willshare.network.AddressAndPort;
import com.sskorupski.willshare.network.BroadcastService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(AndroidJUnit4.class)
public class BroadcastServiceTest {

    private BroadcastService broadcastService;

    @Before
    public void setUp() throws Exception {
        broadcastService = new BroadcastService();
    }

    @Test
    public void sendWillServerResponse() throws Exception {
        AddressAndPort addressAndport = OnClickWillServers.WILL_SERVER_ADDRESS_AND_PORT;
        broadcastService.multicast(addressAndport.getAddress(), addressAndport.getPort(), "Hello client !");
    }
}
