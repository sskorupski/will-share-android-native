package com.sskorupski.willshare.network.okhttp;

import com.squareup.okhttp.OkHttpClient;

public class HttpClient {

    private static OkHttpClient httpClient;

    private HttpClient() {
    }

    public static OkHttpClient getOkHttpClient() {
        if (httpClient == null) {
            httpClient = new OkHttpClient();
        }
        return httpClient;
    }

}
