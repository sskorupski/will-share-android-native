package com.sskorupski.willshare.network.okhttp;

import android.util.Log;

import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;

/**
 * Log les résultats de requête asynchrones
 */
public class LoggerCallback implements Callback {
    public static final String TAG = "LoggerCallback";
    private static LoggerCallback instance;

    private LoggerCallback() {
    }

    public static LoggerCallback getInstance() {
        if (LoggerCallback.instance == null) {
            LoggerCallback.instance = new LoggerCallback();
        }
        return LoggerCallback.instance;
    }

    @Override
    public void onFailure(Request request, IOException e) {
        Log.e(TAG, request.toString(), e);
    }

    @Override
    public void onResponse(Response response) throws IOException {
        Log.i(TAG, String.format("response={%s}", response.toString()));
    }
}
