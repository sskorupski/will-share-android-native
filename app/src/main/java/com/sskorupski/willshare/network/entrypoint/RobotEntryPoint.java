package com.sskorupski.willshare.network.entrypoint;

import java.util.Collection;

public abstract class RobotEntryPoint extends ServerEntryPoint {

    public static final String ROBOT_PATH_SEGMENT = "robot";

    @Override
    public Collection<String> getRelativePaths() {
        Collection<String> relativePaths = super.getRelativePaths();
        relativePaths.add(ROBOT_PATH_SEGMENT);
        return relativePaths;
    }
}
