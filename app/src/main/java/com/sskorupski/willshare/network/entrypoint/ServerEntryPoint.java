package com.sskorupski.willshare.network.entrypoint;

import com.sskorupski.willshare.features.main.servers.SelectedServer;

public abstract class ServerEntryPoint extends EntryPoint {

    @Override
    public String getHost() {
        return SelectedServer.getInstance().getWillServerAddress();
    }

}
