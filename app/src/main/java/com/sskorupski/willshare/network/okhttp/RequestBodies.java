package com.sskorupski.willshare.network.okhttp;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.RequestBody;

public class RequestBodies {

    /**
     * {@link RequestBody} avec un corps de message vide
     */
    public static final RequestBody EMPTY_REQUEST_BODY = RequestBody.create(null, new byte[0]);
    public static final MediaType TEXT_PLAIN = MediaType.parse("text/plain");

    public static RequestBody empty() {
        return EMPTY_REQUEST_BODY;
    }

    public static RequestBody of(String strValue) {
        return RequestBody.create(TEXT_PLAIN, strValue.getBytes());
    }


}
