package com.sskorupski.willshare.network;

import android.view.KeyEvent;

public class AwtKeyEvent {

    /* Virtual key codes. */
    public static final int VK_ENTER = '\n';
    public static final int VK_BACK_SPACE = '\b';
    public static final int VK_ESCAPE = 0x1B;
    public static final int VK_SPACE = 0x20;
    public static final int VK_LEFT = 0x25;
    public static final int VK_UP = 0x26;
    public static final int VK_RIGHT = 0x27;
    public static final int VK_DOWN = 0x28;

    public static int awtKeyCode(int keyCode) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_ENTER:
                return VK_ENTER;
            case KeyEvent.KEYCODE_DEL:
                return VK_BACK_SPACE;
            case KeyEvent.KEYCODE_SPACE:
                return VK_SPACE;
            case KeyEvent.KEYCODE_VOLUME_DOWN:
                return VK_DOWN;
            case KeyEvent.KEYCODE_VOLUME_UP:
                return VK_UP;
        }
        return 0;
    }

    public static int awtKeyCode(KeyEvent keyEvent) {
        return awtKeyCode(keyEvent.getKeyCode());
    }
}
