package com.sskorupski.willshare.network;

import android.util.Log;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.MulticastSocket;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

public class BroadcastService {
    private static final String TAG = "BroadcastService";

    public BroadcastService() {
    }

    /**
     * Iterate through all NetworkInterfaces to find their broadcast address
     */
    public List<InetAddress> listAllBroadcastAddresses() throws SocketException {
        List<InetAddress> broadcastListResult = new ArrayList<>();

        Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
        while (interfaces.hasMoreElements()) {
            NetworkInterface networkInterface = interfaces.nextElement();

            if (networkInterface.isLoopback() || !networkInterface.isUp()) {
                continue;
            }

            for (InterfaceAddress interfaceAddress : networkInterface.getInterfaceAddresses()) {
                if (interfaceAddress.getBroadcast() != null) {
                    broadcastListResult.add(interfaceAddress.getBroadcast());
                }
            }
        }

        return broadcastListResult;
    }


    public void multicast(String inetAddress, Integer port, String multicastMessage) throws IOException {
        InetAddress group = InetAddress.getByName(inetAddress);
        MulticastSocket socket = new MulticastSocket(port);
        socket.joinGroup(group);
        DatagramPacket hi = new DatagramPacket(multicastMessage.getBytes(), multicastMessage.length(),
                group, port);
        socket.send(hi);

        Log.i(TAG, String.format("address={%s:%d} sentMessage={%s}", inetAddress, port, multicastMessage));
    }

}