package com.sskorupski.willshare.network;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AsyncResponseComposite<T> implements AsyncResponse<T> {

    private final List<AsyncResponse<T>> asyncResponseList;

    public AsyncResponseComposite(AsyncResponse<T>... asyncResponses) {
        if (asyncResponses != null && asyncResponses.length > 0) {
            this.asyncResponseList = new ArrayList<>(Arrays.asList(asyncResponses));
        } else {
            this.asyncResponseList = new ArrayList<>();
        }
    }

    public void onAsyncResponse(T response) {
        for (AsyncResponse<T> asyncResponse : asyncResponseList) {
            asyncResponse.onAsyncResponse(response);
        }
    }

    public void add(AsyncResponse<T> asyncResponse) {
        this.asyncResponseList.add(asyncResponse);
    }
}