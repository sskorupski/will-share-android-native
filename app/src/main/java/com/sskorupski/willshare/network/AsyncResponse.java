package com.sskorupski.willshare.network;

public interface AsyncResponse<T> {
    /**
     * Effectue le traitement associé à la <code>response</code> reçue
     *
     * @param response
     */
    void onAsyncResponse(T response);
}