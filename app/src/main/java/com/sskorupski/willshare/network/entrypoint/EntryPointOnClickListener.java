package com.sskorupski.willshare.network.entrypoint;

import android.view.View;

import com.sskorupski.willshare.features.main.model.IMenuItem;

public class EntryPointOnClickListener implements View.OnClickListener {

    private EntryPoint entryPoint;
    protected IMenuItem menuItem;

    public EntryPointOnClickListener(IMenuItem menuItem) {
        this(menuItem, null);
    }

    public EntryPointOnClickListener(IMenuItem menuItem, EntryPoint entryPoint) {
        this.entryPoint = entryPoint;
        this.menuItem = menuItem;
        menuItem.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (menuItem.isEnabled() && entryPoint != null) {
            this.entryPoint.callAsync();
        }
    }

    protected void setEntryPoint(EntryPoint entryPoint) {
        this.entryPoint = entryPoint;
    }
}
