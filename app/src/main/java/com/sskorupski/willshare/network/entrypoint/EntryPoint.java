package com.sskorupski.willshare.network.entrypoint;


import com.squareup.okhttp.Callback;
import com.squareup.okhttp.HttpUrl;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import com.sskorupski.willshare.network.HttpMethods;
import com.sskorupski.willshare.network.okhttp.LoggerCallback;
import com.sskorupski.willshare.network.okhttp.RequestBodies;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import static com.sskorupski.willshare.network.okhttp.HttpClient.getOkHttpClient;

public abstract class EntryPoint {

    private Callback callback;

    public EntryPoint() {
        this.callback = LoggerCallback.getInstance();
    }

    protected Request buildRequest() {
        Request.Builder requestBuilder = new Request.Builder()
                .url(buildUrl());

        if (HttpMethods.GET.equalsIgnoreCase(getHttpMethod())) {
            requestBuilder.get();
        } else {
            requestBuilder.method(getHttpMethod(), getRequestBody());
        }

        return requestBuilder.build();
    }

    protected abstract String getHttpMethod();

    private HttpUrl buildUrl() {
        HttpUrl.Builder builder = HttpUrl.parse(getHost()).newBuilder();
        for (String relativePath : getRelativePaths()) {
            builder.addPathSegment(relativePath);
        }
        return builder.build();
    }

    protected abstract String getHost();

    public void callAsync() {
        getOkHttpClient()
                .newCall(buildRequest())
                .enqueue(callback);
    }

    public Response execute() throws IOException {
        return getOkHttpClient()
                .newCall(buildRequest())
                .execute();
    }

    public RequestBody getRequestBody() {
        return RequestBodies.empty();
    }

    public Collection<String> getRelativePaths() {
        return new ArrayList<>();
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }
}
