package com.sskorupski.willshare.features.main.model;

import java.util.List;

public interface IGroupMenuWithQuickButton extends IMenuItemWithQuickButton {

    IMenuItemWithQuickButton addMenuItem(String subMenuLabel);

    IMenuItemWithQuickButton getMenuItem(int subMenuIndex);

    List<IMenuItemWithQuickButton> getMenuItems();

}
