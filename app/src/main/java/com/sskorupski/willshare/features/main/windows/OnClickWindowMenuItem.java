package com.sskorupski.willshare.features.main.windows;

import com.sskorupski.willshare.features.main.keyboard.KeycodeEntryPoint;
import com.sskorupski.willshare.features.main.windows.entrypoint.CloseWindowEntryPoint;
import com.sskorupski.willshare.features.main.windows.entrypoint.MaximizeWindowEntryPoint;
import com.sskorupski.willshare.features.main.windows.entrypoint.MinMaxAllWindowsEntryPoint;
import com.sskorupski.willshare.features.main.windows.entrypoint.MinimizeWindowEntryPoint;
import com.sskorupski.willshare.features.main.windows.entrypoint.MoveWindowEntryPoint;
import com.sskorupski.willshare.features.main.windows.entrypoint.NewWindowEntryPoint;
import com.sskorupski.willshare.features.main.windows.entrypoint.OpenTaskManagerEntryPoint;
import com.sskorupski.willshare.features.main.windows.entrypoint.RefreshWindowEntryPoint;
import com.sskorupski.willshare.network.AwtKeyEvent;
import com.sskorupski.willshare.network.entrypoint.EntryPointOnClickListener;

public class OnClickWindowMenuItem {

    public OnClickWindowMenuItem(WindowGroupMenu windowMenu) {
        new EntryPointOnClickListener(windowMenu.getCloseMenuItem(), new CloseWindowEntryPoint());
        new EntryPointOnClickListener(windowMenu.getMoveLeftMenuItem(), new MoveWindowEntryPoint(AwtKeyEvent.VK_LEFT));
        new EntryPointOnClickListener(windowMenu.getMoveRightMenuItem(), new MoveWindowEntryPoint(AwtKeyEvent.VK_RIGHT));
        new EntryPointOnClickListener(windowMenu.getMinimizeMenuItem(), new MinimizeWindowEntryPoint());
        new EntryPointOnClickListener(windowMenu.getMaximizeMenuItem(), new MaximizeWindowEntryPoint());
        new EntryPointOnClickListener(windowMenu.getMinMaxAllMenuItem(), new MinMaxAllWindowsEntryPoint());
        new EntryPointOnClickListener(windowMenu.getNewWindowMenuItem(), new NewWindowEntryPoint());
        new EntryPointOnClickListener(windowMenu.getRefreshMenuItem(), new RefreshWindowEntryPoint());
        new EntryPointOnClickListener(windowMenu.getTaskManagerMenuItem(), new OpenTaskManagerEntryPoint());
        new EntryPointOnClickListener(windowMenu.getEscapeMenuItem(), new KeycodeEntryPoint(AwtKeyEvent.VK_ESCAPE));
    }


}
