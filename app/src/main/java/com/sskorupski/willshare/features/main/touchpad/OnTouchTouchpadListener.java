package com.sskorupski.willshare.features.main.touchpad;

import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

public class OnTouchTouchpadListener implements OnTouchListener {

    private static final float X_OFFSET = 20f;
    private static final int CLICK_GESTURE_COUNT_THRESHOLD = 5;

    private final MouseMoveEntryPoint mouseMoveEntryPoint;
    private final MouseClickEntryPoint mouseClickEntryPoint;

    private final int touchPadWidth;
    private final int touchPadHeight;

    private float startPositionX;
    private float startPositionY;
    private float lastPositionX;
    private float lastPositionY;

    private int gestureEventCount;

    private MotionEvent motionEvent;

    OnTouchTouchpadListener(int touchPadWidth, int touchPadHeight) {
        this.mouseMoveEntryPoint = new MouseMoveEntryPoint();
        this.mouseClickEntryPoint = new MouseClickEntryPoint();
        this.touchPadWidth = touchPadWidth;
        this.touchPadHeight = touchPadHeight;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        this.motionEvent = event;

        if (isDownAction()) {
            updateStartPosition();
        }

        if (isMove()) {
            sendMouseMove();
            updateLastPosition();
        }

        if (isClick()) {
            sendMouseLeftClick();
        }

        return true;
    }

    private boolean isUpAction() {
        return motionEvent.getActionMasked() == MotionEvent.ACTION_UP;
    }

    private boolean isMoveAction() {
        return motionEvent.getActionMasked() == MotionEvent.ACTION_MOVE;
    }

    private boolean isDownAction() {
        return motionEvent.getActionMasked() == MotionEvent.ACTION_DOWN;
    }

    private boolean isMove() {
        double xOffset = (double) motionEvent.getX() - lastPositionX;
        double yOffset = (double) motionEvent.getY() - lastPositionY;
        return isMoveAction() && xOffset != 0 && yOffset != 0;
    }

    private void sendMouseMove() {
        double xOffset = (double) motionEvent.getX() - lastPositionX;
        double yOffset = (double) motionEvent.getY() - lastPositionY;
        mouseMoveEntryPoint.setRelativePosition(xOffset / touchPadWidth, yOffset / touchPadHeight);
        mouseMoveEntryPoint.callAsync();
    }

    private void sendMouseLeftClick() {
        mouseClickEntryPoint.setMouseButtonEnum(MouseButtonEnum.LEFT);
        mouseClickEntryPoint.callAsync();
    }

    private boolean isClick() {
        return isUpAction() && gestureEventCount <= CLICK_GESTURE_COUNT_THRESHOLD;
    }


    private void updateLastPosition() {
        gestureEventCount++;
        lastPositionX = motionEvent.getX();
        lastPositionY = motionEvent.getY();
    }

    private void updateStartPosition() {
        gestureEventCount = 0;
        startPositionX = motionEvent.getX();
        startPositionY = motionEvent.getY();
        lastPositionX = startPositionX;
        lastPositionY = startPositionY;
    }
}
