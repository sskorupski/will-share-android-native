package com.sskorupski.willshare.features.settings;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.sskorupski.willshare.R;
import com.sskorupski.willshare.features.main.model.IMenuItem;
import com.sskorupski.willshare.features.main.model.IMenuItemWithQuickButton;

import java.util.HashMap;
import java.util.Map;

public class ConfigMenuAdapter extends BaseExpandableListAdapter {
    private final Activity activity;
    private final ConfigMenu configMenu;
    private final Map<IConfigMenuItem, View> groupViews;
    private final AppPreferences appPreferences;

    public ConfigMenuAdapter(Activity activity, ConfigMenu configMenu) {
        this.activity = activity;
        this.configMenu = configMenu;
        this.groupViews = new HashMap<>();
        this.appPreferences = new AppPreferences(activity);
    }

    @Override
    public IConfigMenuItem getChild(int groupPosition, int childPosititon) {
        IConfigMenuItem configChild = null;
        IConfigMenuItem configMenuItem = this.configMenu.get(groupPosition);
        if (configMenuItem instanceof IConfigGroupMenu) {
            configChild = ((IConfigGroupMenu) configMenuItem).getConfigMenuItem(childPosititon);
        }
        return configChild;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = inflateConfigSubMenu();
        }

        initConfigChild(convertView, getChild(groupPosition, childPosition));
        return convertView;
    }


    private View inflateConfigSubMenu() {
        LayoutInflater layoutInflater = (LayoutInflater) this.activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View convertView = layoutInflater.inflate(R.layout.config_menu_item, null);
        return convertView;
    }

    private void initConfigChild(View subMenuView, IConfigMenuItem childMenu) {
        TextView txtListChild = subMenuView.findViewById(R.id.subMenuLabel);
        txtListChild.setText(childMenu.getLabel());
        subMenuView.setEnabled(childMenu.isEnabled());

        initImageViewWithMenuItemDrawableAndColor(subMenuView, childMenu);
        if (childMenu instanceof IConfigMenuItemWithSwitch) {
            initSubConfigMenuSwitchPreference(subMenuView, (IConfigMenuItemWithSwitch) childMenu);
        }
    }

    private void initSubConfigMenuSwitchPreference(View groupView, IConfigMenuItemWithSwitch childMenu) {
        Switch switchPreference = groupView.findViewById(R.id.switchPreference);
        boolean isPreferenceVisible = appPreferences.isPreferenceVisible(
                (IMenuItemWithQuickButton) childMenu.getMenuItem()
        );
        switchPreference.setVisibility(View.VISIBLE);

        childMenu.setSwitchPreference(switchPreference);
        switchPreference.setChecked(isPreferenceVisible);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflateMenuGroup();
        }
        initGroupMenu(convertView, getGroup(groupPosition));
        this.groupViews.put(getGroup(groupPosition), convertView);
        return convertView;
    }

    private View inflateMenuGroup() {
        LayoutInflater layoutInflater = (LayoutInflater) this.activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View convertView = layoutInflater.inflate(R.layout.config_menu_group, null);
        return convertView;
    }

    private void initGroupMenu(View groupView, IConfigMenuItem groupMenu) {
        TextView groupLabelView = groupView.findViewById(R.id.menuGroupLabel);
        groupLabelView.setTypeface(null, Typeface.BOLD);
        groupLabelView.setText(groupMenu.getLabel());

        groupView.setEnabled(groupMenu.isEnabled());

        initImageViewWithMenuItemDrawableAndColor(groupView, groupMenu);
        initGroupSwitchPreference(groupView, groupMenu);
    }

    private void initGroupSwitchPreference(View groupView, IConfigMenuItem groupMenu) {
        if (groupMenu instanceof IConfigGroupMenu) {
            Switch switchPreference = groupView.findViewById(R.id.switchPreference);
            switchPreference.setVisibility(View.GONE);
        } else if (groupMenu instanceof IConfigMenuItemWithSwitch) {
            initSubConfigMenuSwitchPreference(groupView, (IConfigMenuItemWithSwitch) groupMenu);
        }
    }

    private void initImageViewWithMenuItemDrawableAndColor(View menuView, IMenuItem menuItem) {
        ImageView imageView = menuView.findViewById(R.id.menuIcon);
        if (imageView != null) {
            if (menuItem.getDrawableResourceId() != null) {
                imageView.setImageResource(menuItem.getDrawableResourceId());
            }
            if (menuItem.getColorFilter() != null) {
                imageView.setColorFilter(menuItem.getColorFilter());
            }
        }
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        int childrenCount = 0;
        IConfigMenuItem group = this.configMenu.get(groupPosition);
        if (group instanceof IConfigGroupMenu) {
            childrenCount = ((IConfigGroupMenu) group).getConfigMenuItems().size();
        }
        return childrenCount;
    }

    @Override
    public IConfigMenuItem getGroup(int groupPosition) {
        return this.configMenu.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this.configMenu.size();
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    ConfigMenu getConfigMenu() {
        return configMenu;
    }

}