package com.sskorupski.willshare.features.settings;

import android.app.Activity;
import android.view.View;
import android.widget.CompoundButton;

import com.sskorupski.willshare.features.main.model.IMenuItemWithQuickButton;

public class OnCheckedUpdateQuickbuttonVisibility implements CompoundButton.OnCheckedChangeListener {

    private final Activity activity;
    private final IMenuItemWithQuickButton menuItemWithQuickButton;
    private final AppPreferences appPreferences;

    public OnCheckedUpdateQuickbuttonVisibility(Activity activity,
                                                IMenuItemWithQuickButton menuItemWithQuickButton) {
        this.activity = activity;
        this.menuItemWithQuickButton = menuItemWithQuickButton;
        this.appPreferences = new AppPreferences(activity);
        updateQuickButtonVisibility(appPreferences.isPreferenceVisible(menuItemWithQuickButton));
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, final boolean isChecked) {
        updateQuickButtonVisibility(isChecked);
        updateSharedPreference(isChecked);
    }

    private void updateQuickButtonVisibility(final boolean isChecked) {
        if (menuItemWithQuickButton.getQuickButton() != null) {
            this.activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    menuItemWithQuickButton.getQuickButton().setVisibility(isChecked ? View.VISIBLE : View.GONE);
                }
            });
        }
    }

    private void updateSharedPreference(boolean isChecked) {
        appPreferences.setPreferenceVisible(menuItemWithQuickButton, isChecked);
    }
}
