package com.sskorupski.willshare.features.main.browser;

import com.sskorupski.willshare.features.main.browser.favorite.OpenFavoriteEntryPoint;
import com.sskorupski.willshare.features.main.browser.history.HistoryEntryPoint;
import com.sskorupski.willshare.features.main.browser.open.OpenBrowserEntryPoint;
import com.sskorupski.willshare.features.main.browser.page.NextPageEntryPoint;
import com.sskorupski.willshare.features.main.browser.page.PreviousPageEntryPoint;
import com.sskorupski.willshare.features.main.browser.search.SearchEntryPoint;
import com.sskorupski.willshare.features.main.browser.tab.CloseTabEntryPoint;
import com.sskorupski.willshare.features.main.browser.tab.NewTabEntryPoint;
import com.sskorupski.willshare.features.main.browser.tab.RestoreTabEntryPoint;
import com.sskorupski.willshare.features.main.browser.youtube.OpenYoutubeEntryPoint;
import com.sskorupski.willshare.features.main.browser.youtube.PlayYoutubeEntryPoint;
import com.sskorupski.willshare.network.entrypoint.EntryPointOnClickListener;

public class OnClickBrowserSubMenu {

    private final BrowserMenu browserMenu;

    public OnClickBrowserSubMenu(BrowserMenu browserMenu) {
        this.browserMenu = browserMenu;
        new EntryPointOnClickListener(browserMenu.getHistoryMenu(), new HistoryEntryPoint());
        new EntryPointOnClickListener(browserMenu.getYoutubeMenu(), new OpenYoutubeEntryPoint());
        new EntryPointOnClickListener(browserMenu.getPlayMenu(), new PlayYoutubeEntryPoint());
        new EntryPointOnClickListener(browserMenu.getNewTabMenu(), new NewTabEntryPoint());
        new EntryPointOnClickListener(browserMenu.getCloseTabMenu(), new CloseTabEntryPoint());
        new EntryPointOnClickListener(browserMenu.getRestoreTabMenu(), new RestoreTabEntryPoint());
        new EntryPointOnClickListener(browserMenu.getPreviousPageMenu(), new PreviousPageEntryPoint());
        new EntryPointOnClickListener(browserMenu.getNextPageMenu(), new NextPageEntryPoint());
        new EntryPointOnClickListener(browserMenu.getOpenBrowserMenu(), new OpenBrowserEntryPoint());
        new EntryPointOnClickListener(browserMenu.getFavoriteManagerMenu(), new OpenFavoriteEntryPoint());
        new EntryPointOnClickListener(browserMenu.getSearchMenu(), new SearchEntryPoint());
    }

}
