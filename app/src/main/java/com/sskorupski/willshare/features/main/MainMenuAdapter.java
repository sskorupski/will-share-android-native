package com.sskorupski.willshare.features.main;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.sskorupski.willshare.R;
import com.sskorupski.willshare.features.main.model.IGroupMenu;
import com.sskorupski.willshare.features.main.model.IMenuItem;

import java.util.HashMap;
import java.util.Map;

public class MainMenuAdapter extends BaseExpandableListAdapter {
    private final Activity context;
    private final MainMenu mainMenu;
    private final Map<IMenuItem, View> viewsByGroup;

    public MainMenuAdapter(Activity context, MainMenu mainMenu) {
        this.context = context;
        this.mainMenu = mainMenu;
        this.viewsByGroup = new HashMap<>();
    }

    @Override
    public IMenuItem getChild(int groupPosition, int childPosititon) {
        IMenuItem childResult = null;
        IMenuItem group = this.mainMenu.get(groupPosition);
        if (group instanceof IGroupMenu) {
            childResult = ((IGroupMenu) group).getMenuItem(childPosititon);
        }
        return childResult;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = inflateSubMenu();
        }

        initSubMenu(convertView, getChild(groupPosition, childPosition));
        return convertView;
    }


    private View inflateSubMenu() {
        LayoutInflater layoutInflater = (LayoutInflater) this.context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View convertView = layoutInflater.inflate(R.layout.menu_item, null);
        return convertView;
    }

    private void initSubMenu(View subMenuView, IMenuItem childMenu) {
        TextView txtListChild = subMenuView.findViewById(R.id.subMenuLabel);
        txtListChild.setText(childMenu.getLabel());
        subMenuView.setEnabled(childMenu.isEnabled());

        txtListChild.setTextColor(getColor(childMenu));
        initImageViewWithMenuItemDrawableAndColor(subMenuView, childMenu);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflateMenuGroup();
        }

        IMenuItem group = getGroup(groupPosition);
        initGroupMenu(convertView, getGroup(groupPosition));
        this.viewsByGroup.put(group, convertView);

        return convertView;
    }

    private View inflateMenuGroup() {
        LayoutInflater layoutInflater = (LayoutInflater) this.context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View convertView = layoutInflater.inflate(R.layout.menu_group, null);
        return convertView;
    }

    private void initGroupMenu(View groupView, IMenuItem groupMenu) {
        TextView groupLabelView = groupView.findViewById(R.id.menuGroupLabel);
        groupLabelView.setTypeface(null, Typeface.BOLD);
        groupLabelView.setText(groupMenu.getLabel());

        groupView.setEnabled(groupMenu.isEnabled());
        groupLabelView.setTextColor(getColor(groupMenu));

        initImageViewWithMenuItemDrawableAndColor(groupView, groupMenu);
    }

    private void initImageViewWithMenuItemDrawableAndColor(View view, IMenuItem menuItem) {
        ImageView imageView = view.findViewById(R.id.menuIcon);
        if (imageView != null) {
            if (menuItem.getDrawableResourceId() != null) {
                imageView.setImageResource(menuItem.getDrawableResourceId());
            }
            if (menuItem.getColorFilter() != null) {
                imageView.setColorFilter(menuItem.getColorFilter());
            }
        }
    }

    private int getColor(IMenuItem groupView) {
        int color;
        if (groupView.isEnabled()) {
            color = Color.BLACK;
        } else {
            color = Color.GRAY;
        }
        return color;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        int childrenCountResult = 0;
        IMenuItem group = this.mainMenu.get(groupPosition);
        if (group instanceof IGroupMenu) {
            childrenCountResult = ((IGroupMenu) group).getMenuItems().size();
        }
        return childrenCountResult;
    }

    @Override
    public IMenuItem getGroup(int groupPosition) {
        return this.mainMenu.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this.mainMenu.size();
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    MainMenu getMainMenu() {
        return mainMenu;
    }

}