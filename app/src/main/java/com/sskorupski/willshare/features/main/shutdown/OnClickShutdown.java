package com.sskorupski.willshare.features.main.shutdown;

import android.app.Activity;
import android.view.View;

public class OnClickShutdown {

    private static final String DIALOG_TAG = "shutdown server";

    public OnClickShutdown(final Activity activity, ShutdownMenu shutdownMenu) {
        final ConfirmShutdownDialogFragment confirmShutdownDialog = new ConfirmShutdownDialogFragment();
        shutdownMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmShutdownDialog.show(activity.getFragmentManager(), DIALOG_TAG);
            }
        });
    }

}
