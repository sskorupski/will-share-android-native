package com.sskorupski.willshare.features.settings;

import com.sskorupski.willshare.features.main.model.IMenuItem;
import com.sskorupski.willshare.features.main.model.MenuItem;

public class ConfigMenuItem extends MenuItem implements IConfigMenuItem {

    IMenuItem menuItem;

    ConfigMenuItem(IMenuItem menuItem) {
        super(menuItem.getLabel());
        setDrawableResourceId(menuItem.getDrawableResourceId());
        this.menuItem = menuItem;
    }

    @Override
    public IMenuItem getMenuItem() {
        return menuItem;
    }

}
