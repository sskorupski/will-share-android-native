package com.sskorupski.willshare.features.main.browser.tab;

import com.sskorupski.willshare.features.main.browser.BrowserEntryPoint;
import com.sskorupski.willshare.network.HttpMethods;

import java.util.Collection;


public class CloseTabEntryPoint extends BrowserEntryPoint {
    public static final String ENTRY_POINT_RELATIVE_PATH = "close-tab";

    @Override
    public Collection<String> getRelativePaths() {
        Collection<String> relativePaths = super.getRelativePaths();
        relativePaths.add(ENTRY_POINT_RELATIVE_PATH);
        return relativePaths;
    }

    @Override
    protected String getHttpMethod() {
        return HttpMethods.DELETE;
    }
}
