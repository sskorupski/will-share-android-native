package com.sskorupski.willshare.features.settings;

import com.sskorupski.willshare.features.main.model.IMenuItemWithQuickButton;

import java.util.List;

public interface IConfigGroupMenu extends IConfigMenuItem {

    IConfigMenuItemWithSwitch addConfigMenuItem(IMenuItemWithQuickButton configMenuItem);

    IConfigMenuItemWithSwitch getConfigMenuItem(int subMenuIndex);

    List<IConfigMenuItemWithSwitch> getConfigMenuItems();

}
