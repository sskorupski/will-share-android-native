package com.sskorupski.willshare.features.main.browser.search;

import com.sskorupski.willshare.features.main.browser.BrowserEntryPoint;
import com.sskorupski.willshare.network.HttpMethods;

import java.util.Collection;

public class SearchEntryPoint extends BrowserEntryPoint {

    public static final String ENTRY_POINT_RELATIVE_PATH = "search";


    @Override
    protected String getHttpMethod() {
        return HttpMethods.PUT;
    }

    @Override
    public Collection<String> getRelativePaths() {
        Collection<String> relativePaths = super.getRelativePaths();
        relativePaths.add(ENTRY_POINT_RELATIVE_PATH);
        return relativePaths;
    }
}
