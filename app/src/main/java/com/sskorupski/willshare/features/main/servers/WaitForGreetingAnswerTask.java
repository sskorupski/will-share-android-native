package com.sskorupski.willshare.features.main.servers;

import android.os.AsyncTask;
import android.util.Log;

import com.sskorupski.willshare.network.AddressAndPort;
import com.sskorupski.willshare.network.AsyncResponse;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.UnknownHostException;

/**
 * Ecoute les messages de broadcast dans l'attente d'une réponse d'un Will Shared server<br>
 * Une fois que la réponse attendue est reçue, l'adresse du serveur est transmise via {@link AsyncResponse}
 *
 * @see #onAsyncResponse
 */
public class WaitForGreetingAnswerTask extends AsyncTask<String, Void, InetAddress> {

    private static final String TAG = "WaitForGreetingAnswer";
    private static final String GREETING_ANSWER = "Hello client !";
    private static final int MAX_MESSAGE_LENGTH = 255;

    private final InetAddress broadcastAddress;
    private final int broadcastPort;

    private MulticastSocket multicastSocket;
    private DatagramPacket packet;
    private final byte[] responseBuffer;
    private String greetingResponse;

    private AsyncResponse<InetAddress> onAsyncResponse;

    public WaitForGreetingAnswerTask(AddressAndPort addressAndPort, AsyncResponse<InetAddress> onAsyncResponse) throws UnknownHostException {
        this.broadcastAddress = InetAddress.getByName(addressAndPort.getAddress());
        this.broadcastPort = addressAndPort.getPort();
        this.onAsyncResponse = onAsyncResponse;
        responseBuffer = new byte[MAX_MESSAGE_LENGTH];
    }

    @Override
    protected InetAddress doInBackground(String... strings) {
        try {
            initMulticastSocket();
            logBroadcastListeningReady();
            while (true) {
                receiveAndRefreshGreetingResponse();
                if (isGreetingResponseReceived()) {
                    onGreetingResponse(getReceivedResponseAddress());
                }
            }
        } catch (IOException e) {
            Log.e(TAG, "Listening error", e);
        }
        return null;
    }

    private void logBroadcastListeningReady() {
        Log.i(TAG, String.format(
                "Listening for Will Servers responses on %s:%d ready",
                broadcastAddress,
                broadcastPort
                )
        );
    }

    private void initMulticastSocket() throws IOException {
        multicastSocket = new MulticastSocket(broadcastPort);
        multicastSocket.joinGroup(broadcastAddress);
    }

    private void receiveAndRefreshGreetingResponse() throws IOException {
        packet = new DatagramPacket(responseBuffer, responseBuffer.length, broadcastAddress, broadcastPort);
        multicastSocket.receive(packet);
        greetingResponse = new String(packet.getData(), 0, packet.getLength());

        logReceivedResponse();
    }

    private void logReceivedResponse() {
        Log.i(TAG, String.format(
                "from={%s} message={%s}",
                getReceivedResponseAddress().toString(),
                greetingResponse)
        );
    }

    private InetAddress getReceivedResponseAddress() {
        return packet.getAddress();
    }

    private boolean isGreetingResponseReceived() {
        return GREETING_ANSWER.equalsIgnoreCase(greetingResponse);
    }

    private void onGreetingResponse(InetAddress receivedResponseAddress) {
        if (onAsyncResponse != null) {
            onAsyncResponse.onAsyncResponse(receivedResponseAddress);
        }
    }

}