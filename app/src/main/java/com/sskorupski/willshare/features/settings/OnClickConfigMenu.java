package com.sskorupski.willshare.features.settings;

import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ImageView;

import com.sskorupski.willshare.MainActivity;
import com.sskorupski.willshare.R;
import com.sskorupski.willshare.exception.WrongConfigurationException;
import com.sskorupski.willshare.features.main.model.IMenuItemWithQuickButton;

import java.util.List;

public class OnClickConfigMenu implements
        ExpandableListView.OnGroupClickListener,
        View.OnClickListener {

    private final ConfigMenu configMenu;
    private final MainActivity activity;
    private final AppPreferences willPreference;
    private DrawerLayout drawerLayout;

    public OnClickConfigMenu(MainActivity activity, ConfigMenuAdapter mainMenuAdapter)
            throws WrongConfigurationException {

        this.configMenu = mainMenuAdapter.getConfigMenu();
        this.activity = activity;
        this.willPreference = new AppPreferences(activity);
        initQuickMenu();
        initOnSwitchPreferences(configMenu);
    }

    private void initOnSwitchPreferences(List<? extends IConfigMenuItem> menuItems) {
        for (IConfigMenuItem configMenuItem : menuItems) {
            if (configMenuItem instanceof IConfigMenuItemWithSwitch) {
                IMenuItemWithQuickButton menuItemWithQuickButton = (IMenuItemWithQuickButton) configMenuItem.getMenuItem();
                IConfigMenuItemWithSwitch configMenuItemWithSwitch = (IConfigMenuItemWithSwitch) configMenuItem;

                configMenuItemWithSwitch.setOnCheckedChangeListener(
                        new OnCheckedUpdateQuickbuttonVisibility(activity, menuItemWithQuickButton));
            }
            if (configMenuItem instanceof IConfigGroupMenu) {
                initOnSwitchPreferences(((IConfigGroupMenu) configMenuItem).getConfigMenuItems());
            }
        }
    }

    private void initQuickMenu() {
        drawerLayout = activity.findViewById(R.id.drawer_layout);
        ImageView menuQuickButton = activity.findViewById(R.id.settingQuickButton);
        menuQuickButton.setOnClickListener(this);
        configMenu.getMainMenu().getOpenSettingsMenu().setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        drawerLayout.openDrawer(Gravity.END);
    }

    @Override
    public boolean onGroupClick(ExpandableListView parent, View groupView, int groupPosition, long id) {
        return configMenu.get(groupPosition).onClick();
    }
}
