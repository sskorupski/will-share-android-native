package com.sskorupski.willshare.features.settings;

import com.sskorupski.willshare.features.main.model.IGroupMenu;
import com.sskorupski.willshare.features.main.model.IMenuItem;
import com.sskorupski.willshare.features.main.model.IMenuItemWithQuickButton;

import java.util.ArrayList;
import java.util.List;

public class ConfigGroupMenu extends ConfigMenuItem implements IConfigGroupMenu {

    private List<IConfigMenuItemWithSwitch> configMenuItems;

    public ConfigGroupMenu(IGroupMenu groupMenu) {
        super(groupMenu);
        this.configMenuItems = new ArrayList<>();
        setEnabled(true);
        for (IMenuItem menuItem : groupMenu.getMenuItems()) {
            if (menuItem instanceof IMenuItemWithQuickButton) {
                addConfigMenuItem((IMenuItemWithQuickButton) menuItem);
            }
        }
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        for (IConfigMenuItemWithSwitch subConfigMenu : configMenuItems) {
            subConfigMenu.setEnabled(enabled);
        }
    }

    @Override
    public IConfigMenuItemWithSwitch addConfigMenuItem(IMenuItemWithQuickButton menuItem) {
        IConfigMenuItemWithSwitch subMenu = new ConfigMenuItemWithSwitch(menuItem);
        configMenuItems.add(subMenu);
        return subMenu;
    }

    public IConfigMenuItemWithSwitch getConfigMenuItem(int subMenuIndex) {
        return configMenuItems.get(subMenuIndex);
    }

    public List<IConfigMenuItemWithSwitch> getConfigMenuItems() {
        return configMenuItems;
    }

}