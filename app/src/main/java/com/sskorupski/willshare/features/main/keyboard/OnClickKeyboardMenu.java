package com.sskorupski.willshare.features.main.keyboard;

import android.app.Activity;
import android.content.Context;
import android.os.IBinder;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

public class OnClickKeyboardMenu {

    private final Activity activity;
    private final KeyboardMenu keyboardMenu;

    public OnClickKeyboardMenu(Activity activity, KeyboardMenu keyboardMenu) {
        this.activity = activity;
        this.keyboardMenu = keyboardMenu;
        initOnclickKeyBoardListener();
    }

    private void initOnclickKeyBoardListener() {
        keyboardMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initInputMethodManager().toggleSoftInputFromWindow(
                        getCurrentFocusWindowToken(),
                        InputMethodManager.SHOW_IMPLICIT, 0);
            }
        });
    }

    private IBinder getCurrentFocusWindowToken() {
        return activity.getCurrentFocus().getWindowToken();
    }

    @NonNull
    private InputMethodManager initInputMethodManager() {
        final InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.setInputMethod(getCurrentFocusWindowToken(), getDefaultInputMethod());
        return inputMethodManager;
    }

    private String getDefaultInputMethod() {
        return Settings.Secure.getString(activity.getContentResolver(), Settings.Secure.DEFAULT_INPUT_METHOD);
    }

}
