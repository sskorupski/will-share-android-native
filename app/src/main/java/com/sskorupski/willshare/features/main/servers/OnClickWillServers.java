package com.sskorupski.willshare.features.main.servers;

import android.app.Activity;
import android.view.View;

import com.sskorupski.willshare.exception.WrongConfigurationException;
import com.sskorupski.willshare.network.AddressAndPort;
import com.sskorupski.willshare.network.AsyncResponse;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Observable;
import java.util.Observer;
import java.util.Timer;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class OnClickWillServers extends Observable implements Observer {

    private static final String TAG = "OnClickWillServers";
    static final AddressAndPort WILL_SERVER_ADDRESS_AND_PORT = new AddressAndPort("230.0.0.0", 8042);
    private static final long SERVER_STATUS_TASK_REPEAT_PERIOD_IN_MS = 6000;


    private final ServerGroupMenu serverGroupMenu;
    private final AsyncResponse<InetAddress> onWillServerResponse;
    private final Activity activity;

    public OnClickWillServers(Activity activity, ServerGroupMenu serverGroupMenu) throws WrongConfigurationException {
        this.activity = activity;
        this.serverGroupMenu = serverGroupMenu;
        onWillServerResponse = buildOnWillServerResponse(serverGroupMenu);
        initWaitForWillServerResponse();
        initSearchServerListener();
        initServerStatusTask();
    }

    private void initServerStatusTask() {
        Timer serverStatusTimer = new Timer("serverStatusTimer", true);
        serverStatusTimer.schedule(
                new ServersStatusTimerTask(activity, serverGroupMenu, this),
                0,
                SERVER_STATUS_TASK_REPEAT_PERIOD_IN_MS
        );
    }

    private AddServerMenuItemAsyncResponse buildOnWillServerResponse(ServerGroupMenu serverGroupMenu) {
        AddServerMenuItemAsyncResponse addServerMenuItemAsyncResponse = new AddServerMenuItemAsyncResponse(activity, serverGroupMenu);
        addServerMenuItemAsyncResponse.addObserver(this);
        return addServerMenuItemAsyncResponse;
    }

    private void initWaitForWillServerResponse() throws WrongConfigurationException {
        try {
            final WaitForGreetingAnswerTask waitForGreetingAnswerTask =
                    new WaitForGreetingAnswerTask(WILL_SERVER_ADDRESS_AND_PORT, onWillServerResponse);

            waitForGreetingAnswerTask.executeOnExecutor(Executors.newSingleThreadExecutor());
        } catch (UnknownHostException e) {
            throw new WrongConfigurationException(e);
        }
    }

    private void initSearchServerListener() {
        final ExecutorService sendGreetingExecutor = Executors.newFixedThreadPool(1);
        serverGroupMenu.getSearchServerMenu().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SendGreetingTask(activity, WILL_SERVER_ADDRESS_AND_PORT)
                        .executeOnExecutor(sendGreetingExecutor);
            }
        });
    }

    @Override
    public void update(Observable o, Object arg) {
        setChanged();
        notifyObservers(arg);
    }
}

