package com.sskorupski.willshare.features.main.windows.entrypoint;

import com.sskorupski.willshare.network.HttpMethods;
import com.sskorupski.willshare.network.entrypoint.RobotEntryPoint;

import java.util.Collection;

public class MoveWindowEntryPoint extends RobotEntryPoint {

    public static final String ENTRY_POINT_RELATIVE_PATH = "move-window";
    private final int direction;

    public MoveWindowEntryPoint(int direction) {
        this.direction = direction;
    }


    @Override
    protected String getHttpMethod() {
        return HttpMethods.PUT;
    }

    @Override
    public Collection<String> getRelativePaths() {
        Collection<String> relativePaths = super.getRelativePaths();
        relativePaths.add(ENTRY_POINT_RELATIVE_PATH);
        relativePaths.add(String.valueOf(direction));
        return relativePaths;
    }

}
