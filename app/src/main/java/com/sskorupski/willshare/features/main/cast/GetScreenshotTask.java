package com.sskorupski.willshare.features.main.cast;

import java.util.TimerTask;

public class GetScreenshotTask extends TimerTask {

    private final CastEntryPoint overviewEntryPoint;

    GetScreenshotTask(OnBitmapReceived onBitmapReceived) {
        this.overviewEntryPoint = new CastEntryPoint(onBitmapReceived);
    }

    @Override
    public void run() {
        overviewEntryPoint.callAsync();
    }

}
