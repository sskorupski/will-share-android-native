package com.sskorupski.willshare.features.main.browser.youtube;

import android.view.KeyEvent;

import com.sskorupski.willshare.features.main.keyboard.KeycodeEntryPoint;

import static com.sskorupski.willshare.network.AwtKeyEvent.awtKeyCode;

public class PlayYoutubeEntryPoint extends KeycodeEntryPoint {

    public PlayYoutubeEntryPoint() {
        setKeyCode(awtKeyCode(KeyEvent.KEYCODE_SPACE));
    }

}
