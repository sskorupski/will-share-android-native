package com.sskorupski.willshare.features.main.servers;

import android.app.Activity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sskorupski.willshare.R;
import com.sskorupski.willshare.features.main.model.GroupMenuWithQuickButtons;
import com.sskorupski.willshare.features.main.model.IMenuItem;
import com.sskorupski.willshare.features.main.model.MenuItemWithQuickButton;
import com.sskorupski.willshare.utils.StringUtils;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class ServerGroupMenu extends GroupMenuWithQuickButtons {

    private final Map<String, IMenuItem> willServersByAddress = new HashMap<>();
    private final IMenuItem searchServerMenu;
    private final Activity activity;

    public ServerGroupMenu(Activity activity) {
        super(null);
        this.activity = activity;
        super.setEnabled(true);

        setLabel(StringUtils.byResourceId(activity, R.string.will_servers));
        this.searchServerMenu = buildSearchWillServerMenuItem(activity);
        setDrawableResourceId(R.drawable.ic_will_share);
    }

    private IMenuItem buildSearchWillServerMenuItem(Activity activity) {
        ImageView quickButton = activity.findViewById(R.id.searchWillServerQuickButton);
        MenuItemWithQuickButton menuItem = super.addMenuItem(
                StringUtils.byResourceId(activity, R.string.searchWillServers),
                quickButton,
                StringUtils.byResourceId(activity, R.string.preference_will_search)
        );
        menuItem.setEnabled(true);
        menuItem.setDrawableResourceId(R.drawable.ic_send_greeting_24dp);
        menuItem.setDefaultVisible(true);
        return menuItem;
    }

    @Override
    public MenuItemWithQuickButton addMenuItem(String menuItemLabel, View quickButton, String preferenceKey) {
        MenuItemWithQuickButton menuItem = super.addMenuItem(
                menuItemLabel,
                quickButton,
                preferenceKey
        );
        menuItem.setEnabled(true);
        menuItem.setDrawableResourceId(R.drawable.ic_location_on_grey_24dp);

        willServersByAddress.put(menuItemLabel, menuItem);

        return menuItem;
    }

    IMenuItem addWillServer(String hostAddress) {
        TextView willServerQuickButton = addWillServerQuickButton();
        return addMenuItem(hostAddress, willServerQuickButton, "will." + hostAddress);
    }

    private TextView addWillServerQuickButton() {
        LinearLayout willServersQuickButtonsLayout = activity.findViewById(R.id.willServersLayout);
        TextView willServerView = new TextView(activity);
        willServersQuickButtonsLayout.addView(willServerView);
        return willServerView;
    }

    IMenuItem getServerMenuItem(String inetAddress) {
        return willServersByAddress.get(inetAddress);
    }

    IMenuItem getSearchServerMenu() {
        return searchServerMenu;
    }

    Collection<IMenuItem> getWillServers() {
        return willServersByAddress.values();
    }

    Set<String> getServerAddresses() {
        return willServersByAddress.keySet();
    }

    boolean containsMenuItem(String hostAddress) {
        return willServersByAddress.containsKey(hostAddress);
    }

    @Override
    public void setEnabled(boolean enabled) {
        // No-ops: Servers can't be disabled
    }

    public void remove(String serverAddress) {
        groupMenu.remove(willServersByAddress.get(serverAddress));
        willServersByAddress.remove(serverAddress);
    }
}