package com.sskorupski.willshare.features.main.windows;

import android.app.Activity;
import android.widget.ImageView;

import com.sskorupski.willshare.R;
import com.sskorupski.willshare.features.main.model.GroupMenuWithQuickButtons;
import com.sskorupski.willshare.features.main.model.IMenuItem;
import com.sskorupski.willshare.features.main.model.MenuItemWithQuickButton;
import com.sskorupski.willshare.utils.StringUtils;

public class WindowGroupMenu extends GroupMenuWithQuickButtons {

    private final IMenuItem closeMenuItem;
    private final IMenuItem moveLeftMenuItem;
    private final IMenuItem moveRightMenuItem;
    private final IMenuItem minimizeMenuItem;
    private final IMenuItem maximizeMenuItem;
    private final IMenuItem minMaxAllMenuItem;
    private final IMenuItem newWindowMenuItem;
    private final IMenuItem refreshMenuItem;
    private final IMenuItem taskManagerMenuItem;
    private final IMenuItem escapeMenuItem;
    private final Activity activity;

    public WindowGroupMenu(Activity activity) {
        super("");
        setLabel(StringUtils.byResourceId(activity, R.string.windows));
        this.activity = activity;

        setDrawableResourceId(R.drawable.ic_desktop_windows_black_24dp);

        escapeMenuItem = buildEscapeMenuItem();
        moveLeftMenuItem = buildMoveLeftMenuItem();
        moveRightMenuItem = buildMoveRightMenuItem();
        closeMenuItem = buildCloseMenuItem();
        minimizeMenuItem = buildMinimizeMenuItem();
        maximizeMenuItem = buildMaximizeMenuItem();
        minMaxAllMenuItem = buildMinMaxMenuItem();
        newWindowMenuItem = buildNewWindowMenuItem();
        refreshMenuItem = buildRefreshMenuItem();
        taskManagerMenuItem = buildTaskManagerMenuItem();
    }

    private IMenuItem buildEscapeMenuItem() {
        ImageView quickButton = activity.findViewById(R.id.escapeQuickButton);
        MenuItemWithQuickButton menuItem = addMenuItem(StringUtils.byResourceId(
                activity, R.string.escape),
                quickButton,
                StringUtils.byResourceId(activity, R.string.preference_window_escape));
        menuItem.setDrawableResourceId(R.drawable.ic_escape_black_24dp);
        menuItem.setDefaultVisible(true);
        return menuItem;
    }

    private IMenuItem buildMinimizeMenuItem() {
        ImageView quickButton = activity.findViewById(R.id.minimizeQuickButton);
        IMenuItem menuItem = addMenuItem(
                StringUtils.byResourceId(activity, R.string.minimize),
                quickButton,
                StringUtils.byResourceId(activity, R.string.preference_window_minimize));
        menuItem.setDrawableResourceId(R.drawable.ic_minimize_black_24dp);
        return menuItem;
    }

    private IMenuItem buildMaximizeMenuItem() {
        ImageView quickButton = activity.findViewById(R.id.maximizeQuickButton);
        IMenuItem menuItem = addMenuItem(
                StringUtils.byResourceId(activity, R.string.maximize),
                quickButton,
                StringUtils.byResourceId(activity, R.string.preference_window_maximize));
        menuItem.setDrawableResourceId(R.drawable.ic_maximize_black_24dp);
        return menuItem;
    }

    private IMenuItem buildMinMaxMenuItem() {
        ImageView quickButton = activity.findViewById(R.id.minMaxAllQuickButton);
        IMenuItem menuItem = addMenuItem(
                StringUtils.byResourceId(activity, R.string.minMaxAll),
                quickButton,
                StringUtils.byResourceId(activity, R.string.preference_window_min_max));
        menuItem.setDrawableResourceId(R.drawable.ic_min_max_all_black_24dp);
        return menuItem;
    }


    private IMenuItem buildNewWindowMenuItem() {
        ImageView quickButton = activity.findViewById(R.id.newWindowQuickButton);
        IMenuItem menuItem = addMenuItem(
                StringUtils.byResourceId(activity, R.string.newWindow),
                quickButton,
                StringUtils.byResourceId(activity, R.string.preference_window_new));
        menuItem.setDrawableResourceId(R.drawable.ic_new_window_black_24dp);
        return menuItem;
    }

    private IMenuItem buildRefreshMenuItem() {
        ImageView quickButton = activity.findViewById(R.id.refreshQuickButton);
        IMenuItem menuItem = addMenuItem(
                StringUtils.byResourceId(activity, R.string.refresh),
                quickButton,
                StringUtils.byResourceId(activity, R.string.preference_window_refresh));
        menuItem.setDrawableResourceId(R.drawable.ic_refresh_black_24dp);
        return menuItem;
    }

    private IMenuItem buildTaskManagerMenuItem() {
        ImageView quickButton = activity.findViewById(R.id.taskManagerQuickButton);
        IMenuItem menuItem = addMenuItem(
                StringUtils.byResourceId(activity, R.string.taskManager),
                quickButton,
                StringUtils.byResourceId(activity, R.string.preference_window_task_manager));
        menuItem.setDrawableResourceId(R.drawable.ic_task_manager_black_24dp);
        return menuItem;
    }

    private IMenuItem buildMoveRightMenuItem() {
        ImageView quickButton = activity.findViewById(R.id.moveRightQuickButton);
        IMenuItem menuItem = addMenuItem(
                StringUtils.byResourceId(activity, R.string.move_right),
                quickButton,
                StringUtils.byResourceId(activity, R.string.preference_window_move_right));
        menuItem.setDrawableResourceId(R.drawable.ic_move_right_black_24dp);
        return menuItem;
    }

    private IMenuItem buildMoveLeftMenuItem() {
        ImageView quickButton = activity.findViewById(R.id.moveLeftQuickButton);
        IMenuItem menuItem = addMenuItem(
                StringUtils.byResourceId(activity, R.string.move_left),
                quickButton,
                StringUtils.byResourceId(activity, R.string.preference_window_move_left));
        menuItem.setDrawableResourceId(R.drawable.ic_move_left_black_24dp);
        return menuItem;
    }

    private IMenuItem buildCloseMenuItem() {
        ImageView quickButton = activity.findViewById(R.id.closeQuickButton);
        MenuItemWithQuickButton menuItem = addMenuItem(
                StringUtils.byResourceId(activity, R.string.close),
                quickButton,
                StringUtils.byResourceId(activity, R.string.preference_window_close));
        menuItem.setDrawableResourceId(R.drawable.ic_close_black_24dp);
        menuItem.setDefaultVisible(true);
        return menuItem;
    }

    IMenuItem getCloseMenuItem() {
        return closeMenuItem;
    }

    IMenuItem getMoveLeftMenuItem() {
        return moveLeftMenuItem;
    }

    IMenuItem getMoveRightMenuItem() {
        return moveRightMenuItem;
    }

    IMenuItem getMinimizeMenuItem() {
        return minimizeMenuItem;
    }

    IMenuItem getMaximizeMenuItem() {
        return maximizeMenuItem;
    }

    IMenuItem getMinMaxAllMenuItem() {
        return minMaxAllMenuItem;
    }

    IMenuItem getNewWindowMenuItem() {
        return newWindowMenuItem;
    }

    IMenuItem getRefreshMenuItem() {
        return refreshMenuItem;
    }

    IMenuItem getTaskManagerMenuItem() {
        return taskManagerMenuItem;
    }

    public IMenuItem getEscapeMenuItem() {
        return escapeMenuItem;
    }
}