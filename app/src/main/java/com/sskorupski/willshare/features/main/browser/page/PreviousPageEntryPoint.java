package com.sskorupski.willshare.features.main.browser.page;

import com.sskorupski.willshare.features.main.browser.BrowserEntryPoint;
import com.sskorupski.willshare.network.HttpMethods;

import java.util.Collection;


public class PreviousPageEntryPoint extends BrowserEntryPoint {
    public static final String ENTRY_POINT_RELATIVE_PATH = "previous-page";

    @Override
    public Collection<String> getRelativePaths() {
        Collection<String> relativePaths = super.getRelativePaths();
        relativePaths.add(ENTRY_POINT_RELATIVE_PATH);
        return relativePaths;
    }

    @Override
    protected String getHttpMethod() {
        return HttpMethods.PUT;
    }
}
