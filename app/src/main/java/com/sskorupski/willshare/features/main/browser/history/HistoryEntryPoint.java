package com.sskorupski.willshare.features.main.browser.history;

import com.sskorupski.willshare.features.main.browser.BrowserEntryPoint;
import com.sskorupski.willshare.network.HttpMethods;

import java.util.Collection;

public class HistoryEntryPoint extends BrowserEntryPoint {

    public static final String ENTRY_POINT_RELATIVE_PATH = "history";

    @Override
    protected String getHttpMethod() {
        return HttpMethods.POST;
    }

    @Override
    public Collection<String> getRelativePaths() {
        Collection<String> relativePaths = super.getRelativePaths();
        relativePaths.add(ENTRY_POINT_RELATIVE_PATH);
        return relativePaths;
    }
}
