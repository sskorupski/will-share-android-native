package com.sskorupski.willshare.features.main.browser;

import android.app.Activity;
import android.widget.ImageView;

import com.sskorupski.willshare.R;
import com.sskorupski.willshare.features.main.model.GroupMenuWithQuickButtons;
import com.sskorupski.willshare.features.main.model.IMenuItem;
import com.sskorupski.willshare.utils.StringUtils;

public class BrowserMenu extends GroupMenuWithQuickButtons {

    private final IMenuItem openBrowserMenu;
    private final IMenuItem historyMenu;
    private final IMenuItem youtubeMenu;
    private final IMenuItem playMenu;
    private final IMenuItem previousMenu;
    private final IMenuItem nextMenu;
    private final IMenuItem newTabMenu;
    private final IMenuItem closeTabMenu;
    private final IMenuItem restoreTabMenu;
    private final IMenuItem favoriteManagerMenu;
    private final IMenuItem searchMenu;
    private final Activity activity;

    public BrowserMenu(Activity activity) {
        super(null);
        this.activity = activity;
        setDrawableResourceId(R.drawable.ic_browser_black_24dp);
        setLabel(StringUtils.byResourceId(activity, R.string.browser));

        openBrowserMenu = buildRunMenuItem();
        historyMenu = buildHistoryMenuItem();
        previousMenu = buildPreviousMenuItem();
        nextMenu = buildNextMenuItem();
        newTabMenu = buildNewTabMenuItem();
        restoreTabMenu = buildRestoreTabMenuItem();
        closeTabMenu = buildCloseTabMenuItem();
        youtubeMenu = buildYoutubeMenuItem();
        playMenu = buildPlayMenuItem();
        favoriteManagerMenu = buildFavoriteMenuItem();
        searchMenu = buildSearchMenuItem();
    }


    private IMenuItem buildRunMenuItem() {
        ImageView quickButton = activity.findViewById(R.id.browserQuickButton);
        IMenuItem menuItem = addMenuItem(
                StringUtils.byResourceId(activity, R.string.launchBrowser),
                quickButton,
                StringUtils.byResourceId(activity, R.string.preference_browser_run)
        );
        menuItem.setDrawableResourceId(R.drawable.ic_browser_black_24dp);
        return menuItem;
    }

    private IMenuItem buildRestoreTabMenuItem() {
        ImageView quickButton = activity.findViewById(R.id.restoreTabQuickButton);
        IMenuItem menuItem = addMenuItem(
                StringUtils.byResourceId(activity, R.string.restoreTab),
                quickButton,
                StringUtils.byResourceId(activity, R.string.preference_browser_restore)
        );
        menuItem.setDrawableResourceId(R.drawable.ic_restore_tab_black_24dp);
        return menuItem;
    }

    private IMenuItem buildCloseTabMenuItem() {
        ImageView quickButton = activity.findViewById(R.id.closeTabQuickButton);
        IMenuItem menuItem = addMenuItem(
                StringUtils.byResourceId(activity, R.string.closeTab),
                quickButton,
                StringUtils.byResourceId(activity, R.string.preference_browser_close_tab)
        );
        menuItem.setDrawableResourceId(R.drawable.ic_close_tab_black_24dp);
        return menuItem;
    }

    private IMenuItem buildNewTabMenuItem() {
        ImageView quickButton = activity.findViewById(R.id.newTabQuickButton);
        IMenuItem menuItem = addMenuItem(
                StringUtils.byResourceId(activity, R.string.newTab),
                quickButton,
                StringUtils.byResourceId(activity, R.string.preference_browser_new_tab)
        );
        menuItem.setDrawableResourceId(R.drawable.ic_new_tab_black_24dp);
        return menuItem;
    }

    private IMenuItem buildNextMenuItem() {
        ImageView quickButton = activity.findViewById(R.id.nextQuickButton);
        IMenuItem menuItem = addMenuItem(
                StringUtils.byResourceId(activity, R.string.nextPage),
                quickButton,
                StringUtils.byResourceId(activity, R.string.preference_browser_next_page)
        );
        menuItem.setDrawableResourceId(R.drawable.ic_next_black_24dp);
        return menuItem;
    }

    private IMenuItem buildPreviousMenuItem() {
        ImageView quickButton = activity.findViewById(R.id.previousQuickButton);
        IMenuItem menuItem = addMenuItem(
                StringUtils.byResourceId(activity, R.string.previousPage),
                quickButton,
                StringUtils.byResourceId(activity, R.string.preference_browser_previous_page)
        );
        menuItem.setDrawableResourceId(R.drawable.ic_previous_black_24dp);
        return menuItem;
    }

    private IMenuItem buildFavoriteMenuItem() {
        ImageView quickButton = activity.findViewById(R.id.favoriteQuickButton);
        IMenuItem menuItem = addMenuItem(
                StringUtils.byResourceId(activity, R.string.favorite),
                quickButton,
                StringUtils.byResourceId(activity, R.string.preference_favorite_manager)
        );
        menuItem.setDrawableResourceId(R.drawable.ic_favorite_black_24dp);
        return menuItem;
    }

    private IMenuItem buildPlayMenuItem() {
        IMenuItem menuItem = addMenuItem(StringUtils.byResourceId(activity, R.string.play));
        menuItem.setDrawableResourceId(R.drawable.ic_play_circle_filled_red_24dp);
        return menuItem;
    }

    private IMenuItem buildYoutubeMenuItem() {
        IMenuItem menuItem = addMenuItem(StringUtils.byResourceId(activity, R.string.launchBrowser));
        menuItem.setDrawableResourceId(R.drawable.ic_play_circle_filled_red_24dp);
        return menuItem;
    }

    private IMenuItem buildHistoryMenuItem() {
        ImageView quickButton = activity.findViewById(R.id.historyQuickButton);
        IMenuItem menuItem = addMenuItem(
                StringUtils.byResourceId(activity, R.string.history),
                quickButton,
                StringUtils.byResourceId(activity, R.string.preference_browser_history)
        );
        menuItem.setDrawableResourceId(R.drawable.ic_history_black_24dp);

        return menuItem;
    }

    private IMenuItem buildSearchMenuItem() {
        ImageView quickButton = activity.findViewById(R.id.searchQuickButton);
        IMenuItem menuItem = addMenuItem(
                StringUtils.byResourceId(activity, R.string.search),
                quickButton,
                StringUtils.byResourceId(activity, R.string.preference_browser_search)
        );
        menuItem.setDrawableResourceId(R.drawable.ic_search_black_24dp);

        return menuItem;
    }

    IMenuItem getOpenBrowserMenu() {
        return openBrowserMenu;
    }

    IMenuItem getHistoryMenu() {
        return historyMenu;
    }

    IMenuItem getYoutubeMenu() {
        return youtubeMenu;
    }

    IMenuItem getPlayMenu() {
        return playMenu;
    }

    IMenuItem getPreviousPageMenu() {
        return previousMenu;
    }

    IMenuItem getNextPageMenu() {
        return nextMenu;
    }

    IMenuItem getNewTabMenu() {
        return newTabMenu;
    }

    IMenuItem getCloseTabMenu() {
        return closeTabMenu;
    }

    IMenuItem getRestoreTabMenu() {
        return restoreTabMenu;
    }

    public IMenuItem getFavoriteManagerMenu() {
        return favoriteManagerMenu;
    }

    public IMenuItem getSearchMenu() {
        return searchMenu;
    }
}
