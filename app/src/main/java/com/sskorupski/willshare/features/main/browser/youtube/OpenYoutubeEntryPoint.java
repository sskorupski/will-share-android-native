package com.sskorupski.willshare.features.main.browser.youtube;

import com.sskorupski.willshare.features.main.browser.open.OpenBrowserEntryPoint;

public class OpenYoutubeEntryPoint extends OpenBrowserEntryPoint {

    private static final String YOUTUBE_URL = "http://www.youtube.com";

    public OpenYoutubeEntryPoint() {
        super(YOUTUBE_URL);
    }

}
