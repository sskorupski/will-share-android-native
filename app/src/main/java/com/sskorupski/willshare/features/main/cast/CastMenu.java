package com.sskorupski.willshare.features.main.cast;

import android.app.Activity;

import com.sskorupski.willshare.R;
import com.sskorupski.willshare.features.main.model.MenuItemWithQuickButton;
import com.sskorupski.willshare.utils.StringUtils;


public class CastMenu extends MenuItemWithQuickButton {

    public CastMenu(Activity activity) {
        super(
                StringUtils.byResourceId(activity, R.string.cast),
                activity.findViewById(R.id.castQuickButton),
                StringUtils.byResourceId(activity, R.string.preference_cast)
        );
        setDrawableResourceId(R.drawable.ic_cast_connected_black_24dp);
    }

}
