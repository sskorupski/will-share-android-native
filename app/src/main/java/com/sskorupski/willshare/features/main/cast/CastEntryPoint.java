package com.sskorupski.willshare.features.main.cast;

import android.graphics.BitmapFactory;

import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.sskorupski.willshare.network.HttpMethods;
import com.sskorupski.willshare.network.entrypoint.RobotEntryPoint;

import java.io.IOException;
import java.util.Collection;

import static com.sskorupski.willshare.network.okhttp.HttpClient.getOkHttpClient;

public class CastEntryPoint extends RobotEntryPoint {

    public static final String ENTRY_POINT_RELATIVE_PATH = "screenshot";
    private OnBitmapReceived onBitmapReceived;

    CastEntryPoint(final OnBitmapReceived onBitmapReceived) {
        this.onBitmapReceived = onBitmapReceived;
    }


    @Override
    protected String getHttpMethod() {
        return HttpMethods.GET;
    }

    @Override
    public Collection<String> getRelativePaths() {
        Collection<String> relativePaths = super.getRelativePaths();
        relativePaths.add(ENTRY_POINT_RELATIVE_PATH);
        return relativePaths;
    }

    @Override
    public void callAsync() {
        getOkHttpClient()
                .newCall(buildRequest())
                .enqueue(new Callback() {
                    @Override
                    public void onFailure(Request request, IOException e) {

                    }

                    @Override
                    public void onResponse(Response response) throws IOException {
                        if (response.code() == 200 && onBitmapReceived != null) {
                            byte[] imageBytes = response.body().bytes();
                            onBitmapReceived.onBitmapReceived(BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length));
                        }
                    }
                });
    }

}
