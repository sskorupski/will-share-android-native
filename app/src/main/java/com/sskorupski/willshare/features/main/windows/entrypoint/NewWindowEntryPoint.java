package com.sskorupski.willshare.features.main.windows.entrypoint;

import com.sskorupski.willshare.network.HttpMethods;
import com.sskorupski.willshare.network.entrypoint.RobotEntryPoint;

import java.util.Collection;

public class NewWindowEntryPoint extends RobotEntryPoint {

    public static final String ENTRY_POINT_RELATIVE_PATH = "new-window";

    @Override
    protected String getHttpMethod() {
        return HttpMethods.POST;
    }

    @Override
    public Collection<String> getRelativePaths() {
        Collection<String> relativePaths = super.getRelativePaths();
        relativePaths.add(ENTRY_POINT_RELATIVE_PATH);
        return relativePaths;
    }

}
