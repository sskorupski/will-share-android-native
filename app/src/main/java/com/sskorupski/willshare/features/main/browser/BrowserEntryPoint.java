package com.sskorupski.willshare.features.main.browser;

import com.squareup.okhttp.Request;
import com.sskorupski.willshare.network.entrypoint.ServerEntryPoint;

import java.util.Collection;

public abstract class BrowserEntryPoint extends ServerEntryPoint {

    public static final String BROWSER_PATH_SEGMENT = "browser";
    public static final String USER_AGENT = "User-Agent";

    private String userAgent;

    @Override
    public Collection<String> getRelativePaths() {
        Collection<String> relativePaths = super.getRelativePaths();
        relativePaths.add(BROWSER_PATH_SEGMENT);
        return relativePaths;
    }

    @Override
    protected Request buildRequest() {
        return super.buildRequest()
                .newBuilder()
                .addHeader(USER_AGENT, userAgent)
                .build();
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

}
