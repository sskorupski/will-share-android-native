package com.sskorupski.willshare.features.main.touchpad;

import com.sskorupski.willshare.network.HttpMethods;
import com.sskorupski.willshare.network.entrypoint.RobotEntryPoint;

import java.util.Collection;

public class MouseClickEntryPoint extends RobotEntryPoint {
    public static final String ENTRY_POINT_RELATIVE_PATH = "click";
    private MouseButtonEnum mouseButtonEnum;

    void setMouseButtonEnum(MouseButtonEnum mouseButtonEnum) {
        this.mouseButtonEnum = mouseButtonEnum;
    }

    @Override
    protected String getHttpMethod() {
        return HttpMethods.PUT;
    }

    @Override
    public Collection<String> getRelativePaths() {
        Collection<String> relativePaths = super.getRelativePaths();
        relativePaths.add(ENTRY_POINT_RELATIVE_PATH);
        relativePaths.add(mouseButtonEnum.getCode());
        return relativePaths;
    }
}
