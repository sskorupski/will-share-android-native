package com.sskorupski.willshare.features.main.servers;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.sskorupski.willshare.R;
import com.sskorupski.willshare.network.AddressAndPort;
import com.sskorupski.willshare.network.BroadcastService;
import com.sskorupski.willshare.utils.StringUtils;

import java.io.IOException;

/**
 * Envoi à destination d'un Will Shared server le message de broadcast permettant au serveur de
 * répondre à son tour permettant ainsi de faire connaître son addresse
 *
 * @see WaitForGreetingAnswerTask
 */
public class SendGreetingTask extends AsyncTask<String, Void, Void> {

    private static final String TAG = "SendGreeting";

    private static final String GREETING = "Hello Will !";

    private final BroadcastService broadcastService;
    private final AddressAndPort addressAndPort;
    private final Activity activity;

    SendGreetingTask(Activity activity, AddressAndPort addressAndPort) {
        this.activity = activity;
        this.broadcastService = new BroadcastService();
        this.addressAndPort = addressAndPort;
    }

    @Override
    protected Void doInBackground(String... strings) {
        try {
            sendMulticast(addressAndPort.getAddress(), addressAndPort.getPort(), GREETING);
        } catch (IOException e) {
            Log.e(TAG, "can't send multicast", e);
        }
        return null;
    }

    private void sendMulticast(String inetAddress, int port, String multicastMessage) throws IOException {
        Log.i(TAG, String.format("sendMulticast={%s:%d} multicastMessage={%s}", inetAddress, port, multicastMessage));
        broadcastService.multicast(inetAddress, port, multicastMessage);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        Toast.makeText(
                activity,
                StringUtils.byResourceId(activity, R.string.toast_greeting_sent),
                Toast.LENGTH_SHORT
        ).show();
    }

}