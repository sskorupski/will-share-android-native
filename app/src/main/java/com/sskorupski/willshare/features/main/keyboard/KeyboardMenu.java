package com.sskorupski.willshare.features.main.keyboard;

import android.app.Activity;

import com.sskorupski.willshare.R;
import com.sskorupski.willshare.features.main.model.MenuItemWithQuickButton;
import com.sskorupski.willshare.utils.StringUtils;

public class KeyboardMenu extends MenuItemWithQuickButton {

    public KeyboardMenu(Activity activity) {
        super(
                StringUtils.byResourceId(activity, R.string.keyboard),
                activity.findViewById(R.id.keyboardQuickButton),
                StringUtils.byResourceId(activity, R.string.preference_keyboard)
        );
        setDrawableResourceId(R.drawable.ic_keyboard_black_24dp);
        setDefaultVisible(true);
    }

}
