package com.sskorupski.willshare.features.main;

import android.app.Activity;

import com.sskorupski.willshare.R;
import com.sskorupski.willshare.features.main.model.MenuItemWithQuickButton;
import com.sskorupski.willshare.utils.StringUtils;

public class OpenSettingsMenu extends MenuItemWithQuickButton {

    public OpenSettingsMenu(Activity activity) {
        super(
                StringUtils.byResourceId(activity, R.string.open_configuration),
                activity.findViewById(R.id.settingQuickButton),
                StringUtils.byResourceId(activity, R.string.preference_settings)
        );
        setDrawableResourceId(R.drawable.ic_settings_black_24dp);
        setDefaultVisible(true);
    }
}
