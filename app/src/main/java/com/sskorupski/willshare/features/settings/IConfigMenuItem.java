package com.sskorupski.willshare.features.settings;

import com.sskorupski.willshare.features.main.model.IMenuItem;

public interface IConfigMenuItem extends IMenuItem {

    IMenuItem getMenuItem();
}
