package com.sskorupski.willshare.features.main.shutdown;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;

import com.sskorupski.willshare.R;

public class ConfirmShutdownDialogFragment extends DialogFragment {

    private final ShutdownEntryPoint shutdownEntryPoint;
    private final DialogInterface.OnClickListener onCancel;
    private final DialogInterface.OnClickListener onConfirm;

    public ConfirmShutdownDialogFragment() {
        this.shutdownEntryPoint = new ShutdownEntryPoint();
        onCancel = buildOnCancel();
        onConfirm = buildOnConfirm();
    }

    private DialogInterface.OnClickListener buildOnCancel() {
        return new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
            }
        };
    }

    private DialogInterface.OnClickListener buildOnConfirm() {
        return new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                shutdownEntryPoint.callAsync();
            }
        };
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.dialog_confirm_shutdown_message)
                .setPositiveButton(R.string.shutdown, onConfirm)
                .setNegativeButton(R.string.cancel, onCancel);
        return builder.create();
    }
}