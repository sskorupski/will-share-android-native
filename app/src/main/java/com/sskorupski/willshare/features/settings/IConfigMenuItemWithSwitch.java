package com.sskorupski.willshare.features.settings;

import android.view.View;
import android.widget.CompoundButton;
import android.widget.Switch;

public interface IConfigMenuItemWithSwitch extends IConfigMenuItem {

    void setSwitchPreference(Switch switchPreference);

    Switch getSwitchPreference();

    void setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener onCheckedChangeListener);

    View getQuickButton();
}

