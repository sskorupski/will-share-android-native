package com.sskorupski.willshare.features.main.model;

import java.util.ArrayList;
import java.util.List;

public class GroupMenu extends MenuItem implements IGroupMenu {

    private List<IMenuItem> menuItems;

    public GroupMenu(String label) {
        super(label);
        this.menuItems = new ArrayList<>();
    }

    public IMenuItem addMenuItem(String subMenuLabel) {
        IMenuItem subMenu = new MenuItem(subMenuLabel);
        menuItems.add(subMenu);
        return subMenu;
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        for (IMenuItem subMenu : menuItems) {
            subMenu.setEnabled(enabled);
        }
    }

    public IMenuItem getMenuItem(int subMenuIndex) {
        return menuItems.get(subMenuIndex);
    }

    public List<IMenuItem> getMenuItems() {
        return menuItems;
    }

    public void remove(IMenuItem serverMenuItem) {
        menuItems.remove(serverMenuItem);
    }
}