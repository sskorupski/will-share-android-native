package com.sskorupski.willshare.features.main.model;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class MenuItemWithQuickButton extends MenuItem implements IMenuItemWithQuickButton {
    private View quickButton;
    private final String preferenceKey;
    private boolean defaultVisibility;

    public MenuItemWithQuickButton(String label, View quickButton, String preferenceKey) {
        super(label);
        setQuickButton(quickButton);
        this.preferenceKey = preferenceKey;
    }


    @Override
    public void setOnClickListener(View.OnClickListener onClickListener) {
        super.setOnClickListener(onClickListener);
        if (quickButton != null) {
            quickButton.setOnClickListener(onClickListener);
        }
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        if (quickButton != null) {
            quickButton.setEnabled(enabled);
        }
    }

    @Override
    public void setDrawableResourceId(Integer drawableResourceId) {
        super.setDrawableResourceId(drawableResourceId);
        if (drawableResourceId != null && quickButton != null && quickButton instanceof ImageView) {
            ((ImageView) quickButton).setImageResource(drawableResourceId);
        }
    }

    @Override
    public void setColorFilter(Integer colorFilter) {
        super.setColorFilter(colorFilter);
        if (quickButton != null && colorFilter != null) {
            if (quickButton instanceof ImageView) {
                ((ImageView) quickButton).setColorFilter(colorFilter);
            } else if (quickButton instanceof TextView) {
                ((TextView) quickButton).setTextColor(colorFilter);
            }
        }
    }

    @Override
    public void setLabel(String label) {
        super.setLabel(label);
        if (label != null && quickButton != null && quickButton instanceof TextView) {
            ((TextView) quickButton).setText(label);
        }
    }


    protected void setQuickButton(View quickButton) {
        this.quickButton = quickButton;
        setColorFilter(getColorFilter());
        setDrawableResourceId(getDrawableResourceId());
        setEnabled(isEnabled());
        setOnClickListener(getOnClickListener());
        setLabel(getLabel());
        setDefaultVisible(isDefaultVisible());
    }

    @Override
    public View getQuickButton() {
        return quickButton;
    }

    @Override
    public String getPreferenceKey() {
        return preferenceKey;
    }

    @Override
    public boolean isDefaultVisible() {
        return defaultVisibility;
    }

    @Override
    public void setDefaultVisible(boolean visible) {
        defaultVisibility = visible;
        if (quickButton != null) {
            quickButton.setVisibility(visible ? View.VISIBLE : View.GONE);
        }
    }
}
