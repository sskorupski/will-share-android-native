package com.sskorupski.willshare.features.main.model;

import android.view.View;

public interface IMenuItem {

    String getLabel();

    void setLabel(String response);

    void setEnabled(boolean enabled);

    boolean isEnabled();

    Integer getDrawableResourceId();

    void setDrawableResourceId(Integer resourceId);

    Integer getColorFilter();

    void setColorFilter(Integer color);

    void setOnClickListener(View.OnClickListener onClickListener);

    boolean onClick();

}
