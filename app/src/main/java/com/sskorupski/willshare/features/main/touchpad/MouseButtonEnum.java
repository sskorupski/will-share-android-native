package com.sskorupski.willshare.features.main.touchpad;


public enum MouseButtonEnum {
    LEFT("left"),
    MIDDLE("middle"),
    RIGHT("right");

    private final String code;

    MouseButtonEnum(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
