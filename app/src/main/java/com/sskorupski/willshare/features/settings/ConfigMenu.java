package com.sskorupski.willshare.features.settings;

import android.widget.ExpandableListView;

import com.sskorupski.willshare.MainActivity;
import com.sskorupski.willshare.R;
import com.sskorupski.willshare.exception.WrongConfigurationException;
import com.sskorupski.willshare.features.main.MainMenu;
import com.sskorupski.willshare.features.main.model.IGroupMenu;
import com.sskorupski.willshare.features.main.model.IMenuItem;
import com.sskorupski.willshare.features.main.model.IMenuItemWithQuickButton;

import java.util.ArrayList;

public class ConfigMenu extends ArrayList<IConfigMenuItem> {

    private final MainActivity activity;
    private final MainMenu mainMenu;
    private final AppPreferences appPreferences;


    public ConfigMenu(MainActivity context, MainMenu mainMenu) throws WrongConfigurationException {
        this.activity = context;
        this.mainMenu = mainMenu;
        this.appPreferences = new AppPreferences(context);
        initConfigMenu();
        initConfigMenuListView();
    }

    private void initConfigMenu() {
        for (IMenuItem menuItem : mainMenu) {
            if (menuItem instanceof IGroupMenu) {
                ConfigGroupMenu configGroupMenu = new ConfigGroupMenu((IGroupMenu) menuItem);
                add(configGroupMenu);
                for (IConfigMenuItemWithSwitch menuItemWithQuickButton : configGroupMenu.getConfigMenuItems()) {
                    initDefaultPreference(menuItemWithQuickButton);
                }
            } else if (menuItem instanceof IMenuItemWithQuickButton) {
                IMenuItemWithQuickButton quickButtonMenuItem = (IMenuItemWithQuickButton) menuItem;
                ConfigMenuItemWithSwitch configMenuItemWithSwitch = new ConfigMenuItemWithSwitch(quickButtonMenuItem);
                add(configMenuItemWithSwitch);
                initDefaultPreference(configMenuItemWithSwitch);
            }
        }
    }

    private void initDefaultPreference(IConfigMenuItemWithSwitch configMenuItemWithSwitch) {
        IMenuItemWithQuickButton menuItemWithQuickButton =
                (IMenuItemWithQuickButton) configMenuItemWithSwitch.getMenuItem();
        if (!appPreferences.contains(menuItemWithQuickButton.getPreferenceKey())) {
            appPreferences.setPreferenceVisible(menuItemWithQuickButton, menuItemWithQuickButton.isDefaultVisible());
        }
    }

    private void initConfigMenuListView() throws WrongConfigurationException {
        ExpandableListView expandableListView = activity.findViewById(R.id.configExpandableListView);

        ConfigMenuAdapter configMenuAdapter = new ConfigMenuAdapter(activity, this);
        expandableListView.setAdapter(configMenuAdapter);

        OnClickConfigMenu onClickConfigMenu = new OnClickConfigMenu(activity, configMenuAdapter);
        expandableListView.setOnGroupClickListener(onClickConfigMenu);
    }

    public MainMenu getMainMenu() {
        return mainMenu;
    }
}
