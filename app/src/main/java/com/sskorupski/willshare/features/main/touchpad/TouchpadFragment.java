package com.sskorupski.willshare.features.main.touchpad;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sskorupski.willshare.R;
import com.sskorupski.willshare.features.main.servers.SelectedServer;

public class TouchpadFragment extends Fragment {
    private static final String TAG = "Touchpad";

    private View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.touchpad, container, false);
        this.view = view;
        refresh();
        return view;
    }

    public void refresh() {
        refreshTouchpadView();
    }

    private void refreshTouchpadView() {
        final View touchpad = view.findViewById(R.id.touchpad);
        touchpad.setEnabled(SelectedServer.getInstance() != null);
        // Traitement post init afin de pouvoir accéder aux mesures
        touchpad.post(new Runnable() {
            @Override
            public void run() {
                OnTouchTouchpadListener onTouchTouchpadListener = new OnTouchTouchpadListener(
                        touchpad.getMeasuredWidth(),
                        touchpad.getMeasuredHeight()
                );

                touchpad.setOnTouchListener(onTouchTouchpadListener);
            }
        });
    }

}