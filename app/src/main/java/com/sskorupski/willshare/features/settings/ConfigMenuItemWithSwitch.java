package com.sskorupski.willshare.features.settings;

import android.view.View;
import android.widget.CompoundButton;
import android.widget.Switch;

import com.sskorupski.willshare.features.main.model.IMenuItemWithQuickButton;

public class ConfigMenuItemWithSwitch extends ConfigMenuItem implements IConfigMenuItemWithSwitch {

    private Switch switchPreference;
    private CompoundButton.OnCheckedChangeListener onCheckedChangeListener;

    public ConfigMenuItemWithSwitch(IMenuItemWithQuickButton menuItem) {
        super(menuItem);
    }

    @Override
    public void setSwitchPreference(Switch switchPreference) {
        this.switchPreference = switchPreference;
        setOnCheckedChangeListener(onCheckedChangeListener);
    }

    @Override
    public Switch getSwitchPreference() {
        return switchPreference;
    }

    @Override
    public void setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener onCheckedChangeListener) {
        this.onCheckedChangeListener = onCheckedChangeListener;
        if (switchPreference != null) {
            switchPreference.setOnCheckedChangeListener(onCheckedChangeListener);
        }
    }

    @Override
    public View getQuickButton() {
        return ((IMenuItemWithQuickButton) getMenuItem()).getQuickButton();
    }
}
