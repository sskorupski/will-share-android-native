package com.sskorupski.willshare.features.main.cast;

import android.graphics.Bitmap;
import android.view.View;
import android.widget.ImageView;

import com.sskorupski.willshare.MainActivity;
import com.sskorupski.willshare.R;
import com.sskorupski.willshare.network.entrypoint.EntryPointOnClickListener;

import java.util.Timer;

public class OnClickCastMenu extends EntryPointOnClickListener implements OnBitmapReceived {
    private static final long SCREENSHOT_TASK_REPEAT_PERIOD_IN_MS = 150;
    private final MainActivity activity;
    private Timer screenshotTimer;


    public OnClickCastMenu(MainActivity activity, CastMenu overviewMenu) {
        super(overviewMenu);
        setEntryPoint(new CastEntryPoint(this));
        this.activity = activity;
    }

    @Override
    public void onClick(View v) {
        toggleOverview();
    }

    private void toggleOverview() {
        if (screenshotTimer == null) {
            screenshotTimer = new Timer("getScreenshotTimer", true);
            screenshotTimer.schedule(new GetScreenshotTask(this), 0, SCREENSHOT_TASK_REPEAT_PERIOD_IN_MS);
            updateCastDrawableResource(R.drawable.ic_cast_black_24dp);
        } else {

            screenshotTimer.cancel();
            screenshotTimer.purge();
            screenshotTimer = null;
            onBitmapReceived(null);
            updateCastDrawableResource(R.drawable.ic_cast_connected_black_24dp);
        }
    }

    private void updateCastDrawableResource(int resourceId) {
        menuItem.setDrawableResourceId(resourceId);
    }

    @Override
    public void onBitmapReceived(final Bitmap image) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ImageView overview = activity.findViewById(R.id.overview);
                if (image != null && screenshotTimer != null) {
                    overview.setVisibility(View.VISIBLE);
                    overview.setImageBitmap(image);
                } else {
                    overview.setVisibility(View.GONE);
                }
            }
        });
    }
}
