package com.sskorupski.willshare.features.main.model;

import android.view.View;

import java.util.List;

public class GroupMenuWithQuickButtons extends MenuItem implements IGroupMenu {

    protected GroupMenu groupMenu;

    public GroupMenuWithQuickButtons(String label) {
        super(label);
        groupMenu = new GroupMenu(label);
    }

    public MenuItemWithQuickButton addMenuItem(String subMenuLabel, View quickButton, String preferenceKey) {
        MenuItemWithQuickButton subMenu = new MenuItemWithQuickButton(subMenuLabel, quickButton, preferenceKey);
        groupMenu.getMenuItems().add(subMenu);
        return subMenu;
    }

    @Override
    public IMenuItem addMenuItem(String subMenuLabel) {
        return groupMenu.addMenuItem(subMenuLabel);
    }

    @Override
    public IMenuItem getMenuItem(int subMenuIndex) {
        return groupMenu.getMenuItem(subMenuIndex);
    }

    @Override
    public List<IMenuItem> getMenuItems() {
        return groupMenu.getMenuItems();
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        groupMenu.setEnabled(enabled);
    }
}