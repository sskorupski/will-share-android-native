package com.sskorupski.willshare.features.main;

import android.widget.ExpandableListView;

import com.sskorupski.willshare.MainActivity;
import com.sskorupski.willshare.R;
import com.sskorupski.willshare.exception.WrongConfigurationException;
import com.sskorupski.willshare.features.main.browser.BrowserMenu;
import com.sskorupski.willshare.features.main.cast.CastMenu;
import com.sskorupski.willshare.features.main.keyboard.KeyboardMenu;
import com.sskorupski.willshare.features.main.model.IMenuItem;
import com.sskorupski.willshare.features.main.servers.ServerGroupMenu;
import com.sskorupski.willshare.features.main.shutdown.ShutdownMenu;
import com.sskorupski.willshare.features.main.windows.WindowGroupMenu;

import java.util.ArrayList;

public class MainMenu extends ArrayList<IMenuItem> {

    private MainActivity activity;
    private BrowserMenu browserMenu;
    private ServerGroupMenu willServersMenu;
    private ShutdownMenu shutdownMenu;
    private CastMenu overviewMenu;
    private KeyboardMenu keyboardMenu;
    private WindowGroupMenu windowsMenu;
    private OpenSettingsMenu openSettingsMenu;

    public MainMenu(MainActivity context) throws WrongConfigurationException {
        this.activity = context;
        initServerMenu();
        initKeyboardMenu();
        initWindowsMenu();
        initBrowserMenu();
        initCastMenu();
        initShutdownMenu();
        initOpenSettingsMenu();
        initMainMenuAdapter();
    }

    private void initWindowsMenu() {
        windowsMenu = new WindowGroupMenu(activity);
        add(windowsMenu);
    }

    private void initServerMenu() {
        willServersMenu = new ServerGroupMenu(activity);
        add(willServersMenu);
    }

    private void initKeyboardMenu() {
        keyboardMenu = new KeyboardMenu(activity);
        add(keyboardMenu);
    }

    private void initBrowserMenu() {
        browserMenu = new BrowserMenu(activity);
        add(browserMenu);
    }

    private void initCastMenu() {
        overviewMenu = new CastMenu(activity);
        add(overviewMenu);
    }

    private void initShutdownMenu() {
        shutdownMenu = new ShutdownMenu(activity);
        add(shutdownMenu);
    }

    private void initOpenSettingsMenu() {
        openSettingsMenu = new OpenSettingsMenu(activity);
        openSettingsMenu.setEnabled(true);
        add(openSettingsMenu);
    }


    private void initMainMenuAdapter() throws WrongConfigurationException {
        ExpandableListView expandableListView = activity.findViewById(R.id.mainExpandableListView);

        MainMenuAdapter mainMenuAdapter = new MainMenuAdapter(activity, this);
        expandableListView.setAdapter(mainMenuAdapter);

        OnClickMainMenu onSelectListener = new OnClickMainMenu(activity, mainMenuAdapter);
        expandableListView.setOnGroupClickListener(onSelectListener);
        expandableListView.setOnChildClickListener(onSelectListener);
    }

    void setEnableAll(boolean enable) {
        for (IMenuItem groupMenu : this) {
            groupMenu.setEnabled(enable);
        }
    }

    public BrowserMenu getBrowserMenu() {
        return browserMenu;
    }

    public ServerGroupMenu getWillServersMenu() {
        return willServersMenu;
    }

    public CastMenu getOverviewMenu() {
        return overviewMenu;
    }

    public ShutdownMenu getShutdownMenu() {
        return shutdownMenu;
    }

    public KeyboardMenu getKeyboardMenu() {
        return keyboardMenu;
    }

    public WindowGroupMenu getWindowsMenu() {
        return windowsMenu;
    }

    public OpenSettingsMenu getOpenSettingsMenu() {
        return openSettingsMenu;
    }

}
