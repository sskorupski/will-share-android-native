package com.sskorupski.willshare.features.main.servers;

import com.sskorupski.willshare.network.HttpMethods;
import com.sskorupski.willshare.network.entrypoint.EntryPoint;

import java.util.Collection;

public class HostnameEntryPoint extends EntryPoint {

    public static final String ENTRY_POINT_RELATIVE_PATH = "hostname";
    private final String host;

    public HostnameEntryPoint(String serverAddress) {
        this.host = "http://" + serverAddress + ":" + OnClickWillServers.WILL_SERVER_ADDRESS_AND_PORT.getPort();
    }

    @Override
    protected String getHttpMethod() {
        return HttpMethods.GET;
    }

    @Override
    protected String getHost() {
        return host;
    }

    @Override
    public Collection<String> getRelativePaths() {
        Collection<String> relativePaths = super.getRelativePaths();
        relativePaths.add(ENTRY_POINT_RELATIVE_PATH);
        return relativePaths;
    }
}
