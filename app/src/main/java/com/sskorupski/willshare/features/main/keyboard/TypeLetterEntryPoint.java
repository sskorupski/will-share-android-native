package com.sskorupski.willshare.features.main.keyboard;

import com.sskorupski.willshare.network.HttpMethods;
import com.sskorupski.willshare.network.entrypoint.RobotEntryPoint;

import java.util.Collection;

public class TypeLetterEntryPoint extends RobotEntryPoint {

    public static final String ENTRY_POINT_RELATIVE_PATH = "type-letter";
    private char letter;

    public TypeLetterEntryPoint() {
    }

    public void setLetter(char letter) {
        this.letter = letter;
    }


    @Override
    protected String getHttpMethod() {
        return HttpMethods.PUT;
    }

    @Override
    public Collection<String> getRelativePaths() {
        Collection<String> relativePaths = super.getRelativePaths();
        relativePaths.add(ENTRY_POINT_RELATIVE_PATH);
        relativePaths.add(String.valueOf(letter));
        return relativePaths;
    }
}
