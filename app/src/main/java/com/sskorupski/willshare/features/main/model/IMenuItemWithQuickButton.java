package com.sskorupski.willshare.features.main.model;

import android.view.View;

public interface IMenuItemWithQuickButton extends IMenuItem {

    View getQuickButton();

    String getPreferenceKey();

    boolean isDefaultVisible();

    void setDefaultVisible(boolean visible);
}
