package com.sskorupski.willshare.features.main.servers;

import android.util.Log;

public class SelectedServer {
    private static final String TAG = "SelectedServer";
    private static SelectedServer instance;

    private String willServerAddress = "http://127.0.0.1";
    private boolean serverSelected = false;

    private SelectedServer() {
    }

    public static SelectedServer getInstance() {
        if (SelectedServer.instance == null) {
            SelectedServer.instance = new SelectedServer();
        }
        return SelectedServer.instance;
    }

    void setWillServerAddress(String willServerAddress) {
        this.willServerAddress = "http://" + willServerAddress + ":" + OnClickWillServers.WILL_SERVER_ADDRESS_AND_PORT.getPort();
        Log.i(TAG, String.format("Selected Will Server={%s}", willServerAddress));
        serverSelected = true;
    }

    public String getWillServerAddress() {
        return willServerAddress;
    }

    public boolean isServerSelected() {
        return serverSelected;
    }

    public void setServerSelected(boolean serverSelected) {
        this.serverSelected = serverSelected;
    }
}
