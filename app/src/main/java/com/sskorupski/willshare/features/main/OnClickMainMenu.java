package com.sskorupski.willshare.features.main;

import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ImageView;

import com.sskorupski.willshare.MainActivity;
import com.sskorupski.willshare.R;
import com.sskorupski.willshare.exception.WrongConfigurationException;
import com.sskorupski.willshare.features.main.browser.OnClickBrowserSubMenu;
import com.sskorupski.willshare.features.main.cast.OnClickCastMenu;
import com.sskorupski.willshare.features.main.keyboard.OnClickKeyboardMenu;
import com.sskorupski.willshare.features.main.model.IGroupMenu;
import com.sskorupski.willshare.features.main.model.IMenuItem;
import com.sskorupski.willshare.features.main.servers.OnClickWillServers;
import com.sskorupski.willshare.features.main.servers.SelectedServer;
import com.sskorupski.willshare.features.main.shutdown.OnClickShutdown;
import com.sskorupski.willshare.features.main.windows.OnClickWindowMenuItem;

import java.util.Observable;
import java.util.Observer;

public class OnClickMainMenu implements
        ExpandableListView.OnGroupClickListener,
        ExpandableListView.OnChildClickListener,
        Observer {

    private final MainMenu mainMenu;
    private final MainActivity activity;
    private final MainMenuAdapter mainMenuAdapter;

    public OnClickMainMenu(MainActivity activity, MainMenuAdapter mainMenuAdapter)
            throws WrongConfigurationException {

        this.mainMenu = mainMenuAdapter.getMainMenu();
        this.mainMenuAdapter = mainMenuAdapter;
        this.activity = activity;
        initOnClickWillServerListeners();
        new OnClickKeyboardMenu(activity, mainMenu.getKeyboardMenu());
        new OnClickWindowMenuItem(mainMenu.getWindowsMenu());
        new OnClickBrowserSubMenu(mainMenu.getBrowserMenu());
        new OnClickCastMenu(activity, mainMenu.getOverviewMenu());
        new OnClickShutdown(activity, mainMenu.getShutdownMenu());
        initQuickMenu();
    }

    private void initQuickMenu() {
        ImageView menuQuickButton = activity.findViewById(R.id.menuQuickButton);
        final DrawerLayout drawerLayout = activity.findViewById(R.id.drawer_layout);
        menuQuickButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.openDrawer(Gravity.START);
            }
        });
    }

    private void initOnClickWillServerListeners() throws WrongConfigurationException {
        OnClickWillServers onClickWillServers = new OnClickWillServers(activity, mainMenu.getWillServersMenu());
        onClickWillServers.addObserver(this);
    }


    @Override
    public boolean onGroupClick(ExpandableListView parent, View groupView, int groupPosition, long id) {
        if (mainMenuAdapter.getGroup(groupPosition).isEnabled()) {
            mainMenu.get(groupPosition).onClick();
            parent.expandGroup(groupPosition);
        }
        return true;
    }

    @Override
    public boolean onChildClick(ExpandableListView parent, View subMenuView, int groupPosition, int childPosition, long id) {
        boolean clickHandled = false;
        IMenuItem group = mainMenu.get(groupPosition);
        if (subMenuView.isEnabled() && group instanceof IGroupMenu) {
            clickHandled = ((IGroupMenu) group).getMenuItem(childPosition).onClick();
        }
        return clickHandled;
    }

    @Override
    public void update(Observable o, Object arg) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mainMenu.setEnableAll(SelectedServer.getInstance().isServerSelected());
                mainMenuAdapter.notifyDataSetChanged();
            }
        });
    }
}
