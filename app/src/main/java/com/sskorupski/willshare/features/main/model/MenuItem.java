package com.sskorupski.willshare.features.main.model;

import android.view.View;

public class MenuItem implements IMenuItem {
    private String label;
    private View.OnClickListener onClickListener;
    private boolean enabled = false;
    private Integer drawableResourceId = null;
    private Integer colorFilter;

    public MenuItem(String label) {
        this.label = label;
    }

    public void setOnClickListener(View.OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    /**
     * @return True if then click was handled
     */
    public boolean onClick() {
        boolean clickHasBeenHandled = false;
        if (this.onClickListener != null) {
            onClickListener.onClick(null);
            clickHasBeenHandled = true;
        }
        return clickHasBeenHandled;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public Integer getDrawableResourceId() {
        return drawableResourceId;
    }

    public void setDrawableResourceId(Integer drawableResourceId) {
        this.drawableResourceId = drawableResourceId;
    }

    public Integer getColorFilter() {
        return colorFilter;
    }

    public void setColorFilter(Integer colorFilter) {
        this.colorFilter = colorFilter;
    }

    View.OnClickListener getOnClickListener() {
        return onClickListener;
    }
}
