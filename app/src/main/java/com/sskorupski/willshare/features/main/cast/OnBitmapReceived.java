package com.sskorupski.willshare.features.main.cast;

import android.graphics.Bitmap;

public interface OnBitmapReceived {
    void onBitmapReceived(Bitmap image);
}
