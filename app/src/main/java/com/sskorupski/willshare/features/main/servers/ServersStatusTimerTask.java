package com.sskorupski.willshare.features.main.servers;

import android.app.Activity;
import android.util.Log;
import android.widget.Toast;

import com.sskorupski.willshare.R;
import com.sskorupski.willshare.features.main.model.IMenuItem;
import com.sskorupski.willshare.utils.StringUtils;

import java.io.IOException;
import java.util.Observer;
import java.util.TimerTask;

public class ServersStatusTimerTask extends TimerTask {

    private static final String TAG = "serverStatutsTask";
    private final ServerGroupMenu serverGroupMenu;
    private final Observer observer;
    private final Activity activity;
    private final SelectedServer selectedServer = SelectedServer.getInstance();

    public ServersStatusTimerTask(Activity activity, ServerGroupMenu serverGroupMenu, Observer observer) {
        this.activity = activity;
        this.serverGroupMenu = serverGroupMenu;
        this.observer = observer;
    }

    @Override
    public void run() {
        for (final String serverAddress : serverGroupMenu.getServerAddresses()) {
            HostnameEntryPoint serverEntryPoint = new HostnameEntryPoint(serverAddress);
            try {
                serverEntryPoint.execute();
            } catch (IOException e) {
                Log.i(TAG, String.format("Server missing %s", serverAddress));
                IMenuItem serverMenu = serverGroupMenu.getServerMenuItem(serverAddress);

                showMissingServerToast(serverMenu.getLabel());
                serverGroupMenu.remove(serverAddress);
                unselectServerIfSelected(serverAddress);

                if (observer != null) {
                    observer.update(null, serverGroupMenu);
                }
            }
        }
    }

    private void unselectServerIfSelected(String serverAddress) {
        if (selectedServer.getWillServerAddress().contains(serverAddress)) {
            selectedServer.setServerSelected(false);
        }
    }

    private void showMissingServerToast(final String serverName) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(
                        activity,
                        serverName + StringUtils.byResourceId(activity, R.string.toast_no_server_response),
                        Toast.LENGTH_SHORT
                ).show();
            }
        });
    }
}
