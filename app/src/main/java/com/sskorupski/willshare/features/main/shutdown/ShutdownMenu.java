package com.sskorupski.willshare.features.main.shutdown;

import android.app.Activity;

import com.sskorupski.willshare.R;
import com.sskorupski.willshare.features.main.model.MenuItemWithQuickButton;
import com.sskorupski.willshare.utils.StringUtils;

public class ShutdownMenu extends MenuItemWithQuickButton {

    public ShutdownMenu(Activity activity) {
        super(
                StringUtils.byResourceId(activity, R.string.shutdown),
                activity.findViewById(R.id.shutdownQuickButton),
                StringUtils.byResourceId(activity, R.string.preference_shutdown)
        );
        setDrawableResourceId(R.drawable.ic_shutdown_black_24dp);
        setDefaultVisible(true);
    }
}
