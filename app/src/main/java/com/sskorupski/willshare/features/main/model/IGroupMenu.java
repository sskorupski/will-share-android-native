package com.sskorupski.willshare.features.main.model;

import java.util.List;

public interface IGroupMenu extends IMenuItem {

    IMenuItem addMenuItem(String subMenuLabel);

    IMenuItem getMenuItem(int subMenuIndex);

    List<IMenuItem> getMenuItems();

}
