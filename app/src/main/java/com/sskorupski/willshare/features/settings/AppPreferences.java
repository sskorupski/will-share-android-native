package com.sskorupski.willshare.features.settings;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.sskorupski.willshare.features.main.model.IMenuItemWithQuickButton;

public class AppPreferences {

    private final SharedPreferences.Editor sharedPreferenceEditor;
    private final SharedPreferences sharedPreferences;

    public AppPreferences(Activity activity) {
        sharedPreferences = activity.getPreferences(Context.MODE_PRIVATE);
        this.sharedPreferenceEditor = activity.getPreferences(Context.MODE_PRIVATE).edit();
    }

    public boolean isPreferenceVisible(IMenuItemWithQuickButton menuItemWithQuickButton) {
        return sharedPreferences.getBoolean(
                menuItemWithQuickButton.getPreferenceKey(),
                false
        );
    }

    public void setPreferenceVisible(
            IMenuItemWithQuickButton menuItemWithQuickButton, boolean isPreferenceVisible) {
        sharedPreferenceEditor.putBoolean(menuItemWithQuickButton.getPreferenceKey(), isPreferenceVisible);
        sharedPreferenceEditor.commit();
    }

    public boolean contains(String preferenceKey) {
        return sharedPreferences.contains(preferenceKey);
    }
}
