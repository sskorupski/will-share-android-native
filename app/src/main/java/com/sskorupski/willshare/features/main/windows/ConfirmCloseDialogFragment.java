package com.sskorupski.willshare.features.main.windows;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;

import com.sskorupski.willshare.R;
import com.sskorupski.willshare.features.main.windows.entrypoint.CloseWindowEntryPoint;

public class ConfirmCloseDialogFragment extends DialogFragment {

    private final CloseWindowEntryPoint closeWindowEntryPoint;
    private final DialogInterface.OnClickListener onCancel;
    private final DialogInterface.OnClickListener onConfirm;

    public ConfirmCloseDialogFragment() {
        this.closeWindowEntryPoint = new CloseWindowEntryPoint();
        onCancel = buildOnCancel();
        onConfirm = buildOnConfirm();
    }

    private DialogInterface.OnClickListener buildOnCancel() {
        return new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
            }
        };
    }

    private DialogInterface.OnClickListener buildOnConfirm() {
        return new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                closeWindowEntryPoint.callAsync();
            }
        };
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.dialog_confirm_close_window_message)
                .setPositiveButton(R.string.close, onConfirm)
                .setNegativeButton(R.string.cancel, onCancel);
        return builder.create();
    }
}