package com.sskorupski.willshare.features.main.touchpad;

import com.sskorupski.willshare.network.HttpMethods;
import com.sskorupski.willshare.network.entrypoint.RobotEntryPoint;

import java.util.Collection;

public class MouseMoveEntryPoint extends RobotEntryPoint {
    public static final String ENTRY_POINT_RELATIVE_PATH = "move-relative";
    private double relativeX;
    private double relativeY;

    public void setRelativePosition(double relativeX, double relativeY) {
        this.relativeX = relativeX;
        this.relativeY = relativeY;
    }


    @Override
    protected String getHttpMethod() {
        return HttpMethods.PUT;
    }

    @Override
    public Collection<String> getRelativePaths() {
        Collection<String> relativePaths = super.getRelativePaths();
        relativePaths.add(ENTRY_POINT_RELATIVE_PATH);
        relativePaths.add(String.valueOf(relativeX));
        relativePaths.add(String.valueOf(relativeY));
        return relativePaths;
    }

}
