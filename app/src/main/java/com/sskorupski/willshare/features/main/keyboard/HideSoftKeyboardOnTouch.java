package com.sskorupski.willshare.features.main.keyboard;

import android.app.Activity;
import android.os.IBinder;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

public class HideSoftKeyboardOnTouch implements View.OnTouchListener {

    private final Activity activity;

    public HideSoftKeyboardOnTouch(Activity activity, View parentView) {
        this.activity = activity;
        initListener(parentView);
    }

    private void initListener(View view) {
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(this);
        }

        if (view instanceof ViewGroup) {
            initListener((ViewGroup) view);
        }
    }

    private void initListener(ViewGroup view) {
        for (int i = 0; i < view.getChildCount(); i++) {
            initListener(view.getChildAt(i));
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        hideSoftKeyboard();
        return false;
    }

    private void hideSoftKeyboard() {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(getCurrentFocusWindowToken(), 0);
    }

    private IBinder getCurrentFocusWindowToken() {
        return activity.getCurrentFocus().getWindowToken();
    }

}
