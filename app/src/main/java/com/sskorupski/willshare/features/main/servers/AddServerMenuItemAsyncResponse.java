package com.sskorupski.willshare.features.main.servers;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.view.View;

import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.sskorupski.willshare.R;
import com.sskorupski.willshare.features.main.model.IMenuItem;
import com.sskorupski.willshare.network.AsyncResponse;

import java.io.IOException;
import java.net.InetAddress;
import java.util.Observable;

public class AddServerMenuItemAsyncResponse extends Observable implements AsyncResponse<InetAddress> {

    private final Activity activity;
    private final ServerGroupMenu serverGroupMenu;
    private IMenuItem selectedWillServerMenu;

    public AddServerMenuItemAsyncResponse(Activity activity, ServerGroupMenu serverGroupMenu) {
        this.activity = activity;
        this.serverGroupMenu = serverGroupMenu;
    }

    @Override
    public void onAsyncResponse(final InetAddress response) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                synchronized (AddServerMenuItemAsyncResponse.this) {
                    addServerMenuItem(response);
                }
            }
        });
    }

    private void addServerMenuItem(InetAddress inetAddress) {
        if (!serverGroupMenu.containsMenuItem(inetAddress.getHostAddress())) {
            final IMenuItem serverMenuItem = serverGroupMenu.addWillServer(inetAddress.getHostAddress());
            serverMenuItem.setEnabled(true);
            View.OnClickListener onSelectWillServerListener = buildOnSelectWillServer(inetAddress, serverMenuItem);

            setChanged();
            notifyObservers();

            serverMenuItem.setOnClickListener(onSelectWillServerListener);
            callGetHostnameEntryPoint(inetAddress);
        }
    }

    private View.OnClickListener buildOnSelectWillServer(final InetAddress serverAddress, final IMenuItem serverMenuItem) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateSelectedWillServer(serverMenuItem, serverAddress);
                updateWillServerView();
                setChanged();
                notifyObservers();
            }
        };
    }

    private void updateSelectedWillServer(IMenuItem serverMenuItem, InetAddress serverAddress) {
        selectedWillServerMenu = serverMenuItem;
        SelectedServer.getInstance().setWillServerAddress(serverAddress.getHostAddress());
    }

    private void updateWillServerView() {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                for (IMenuItem menuItem : serverGroupMenu.getWillServers()) {
                    if (menuItem == selectedWillServerMenu) {
                        menuItem.setDrawableResourceId(R.drawable.ic_location_on_green_24dp);
                        menuItem.setColorFilter(ContextCompat.getColor(activity, R.color.colorGreen));
                    } else {
                        menuItem.setDrawableResourceId(R.drawable.ic_location_on_grey_24dp);
                        menuItem.setColorFilter(ContextCompat.getColor(activity, R.color.colorGray));
                    }
                }
                setChanged();
                notifyObservers(serverGroupMenu.getWillServers());
            }
        });
    }

    private void callGetHostnameEntryPoint(final InetAddress inetAddress) {
        HostnameEntryPoint hostnameEntryPoint = new HostnameEntryPoint(inetAddress.getHostAddress());
        hostnameEntryPoint.setCallback(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
            }

            @Override
            public void onResponse(final Response hostnameResponse) throws IOException {
                updateServerLabel(hostnameResponse, inetAddress);
            }
        });
        hostnameEntryPoint.callAsync();
    }

    @NonNull
    private void updateServerLabel(final Response hostnameResponse, final InetAddress serverAddress) throws IOException {
        final String body = hostnameResponse.body().string();
        if (body != null && body.length() != 0) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    IMenuItem serverToUpdate = serverGroupMenu.getServerMenuItem(serverAddress.getHostAddress());
                    serverToUpdate.setLabel(body);
                    setChanged();
                    notifyObservers(serverToUpdate);
                }
            });
        }
    }

}
