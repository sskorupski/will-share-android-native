package com.sskorupski.willshare.features.main.browser.open;

import com.squareup.okhttp.RequestBody;
import com.sskorupski.willshare.features.main.browser.BrowserEntryPoint;
import com.sskorupski.willshare.network.HttpMethods;
import com.sskorupski.willshare.network.okhttp.RequestBodies;

import java.util.Collection;

public class OpenBrowserEntryPoint extends BrowserEntryPoint {

    public static final String ENTRY_POINT_RELATIVE_PATH = "browser";
    private final String openUrl;

    public OpenBrowserEntryPoint() {
        this(null);
    }

    public OpenBrowserEntryPoint(String openUrl) {
        this.openUrl = openUrl;
    }


    @Override
    protected String getHttpMethod() {
        return HttpMethods.POST;
    }

    @Override
    public Collection<String> getRelativePaths() {
        Collection<String> relativePaths = super.getRelativePaths();
        relativePaths.add(ENTRY_POINT_RELATIVE_PATH);
        return relativePaths;
    }

    @Override
    public RequestBody getRequestBody() {
        RequestBody requestBody;
        if (openUrl == null || openUrl.length() == 0) {
            requestBody = RequestBodies.empty();
        } else {
            requestBody = RequestBodies.of(openUrl);
        }
        return requestBody;
    }
}
