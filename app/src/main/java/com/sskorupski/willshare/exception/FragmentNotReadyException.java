package com.sskorupski.willshare.exception;

public class FragmentNotReadyException extends Exception {

    public FragmentNotReadyException() {
    }

    public FragmentNotReadyException(String message) {
        super(message);
    }

    public FragmentNotReadyException(String message, Throwable cause) {
        super(message, cause);
    }

    public FragmentNotReadyException(Throwable cause) {
        super(cause);
    }

}
