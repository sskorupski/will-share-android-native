package com.sskorupski.willshare.exception;

public class WrongConfigurationException extends Exception {
    public WrongConfigurationException() {
    }

    public WrongConfigurationException(String message) {
        super(message);
    }

    public WrongConfigurationException(String message, Throwable cause) {
        super(message, cause);
    }

    public WrongConfigurationException(Throwable cause) {
        super(cause);
    }

}
