package com.sskorupski.willshare.event;

import android.view.View;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class OnClickListenerComposite implements View.OnClickListener {

    private List<View.OnClickListener> onClickListeners;

    public OnClickListenerComposite(View.OnClickListener... onClickListeners) {
        if (onClickListeners != null && onClickListeners.length > 0) {
            this.onClickListeners = new ArrayList<>(Arrays.asList(onClickListeners));
        } else {
            this.onClickListeners = new ArrayList<>();
        }
    }

    @Override
    public void onClick(View v) {
        for (View.OnClickListener onClickListener : this.onClickListeners) {
            onClickListener.onClick(v);
        }
    }

    public void add(View.OnClickListener onClickListener) {
        this.onClickListeners.add(onClickListener);
    }

}
