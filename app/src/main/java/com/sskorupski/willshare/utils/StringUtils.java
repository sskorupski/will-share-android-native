package com.sskorupski.willshare.utils;

import android.app.Activity;
import android.support.annotation.NonNull;

public class StringUtils {

    @NonNull
    public static String byResourceId(Activity activity, int stringResourceId) {
        return activity.getResources().getString(stringResourceId);
    }
}
