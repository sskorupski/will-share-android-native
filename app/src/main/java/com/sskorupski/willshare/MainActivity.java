package com.sskorupski.willshare;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;

import com.sskorupski.willshare.exception.WrongConfigurationException;
import com.sskorupski.willshare.features.main.MainMenu;
import com.sskorupski.willshare.features.main.keyboard.HideSoftKeyboardOnTouch;
import com.sskorupski.willshare.features.main.keyboard.KeycodeEntryPoint;
import com.sskorupski.willshare.features.main.keyboard.TypeLetterEntryPoint;
import com.sskorupski.willshare.features.settings.ConfigMenu;

import static com.sskorupski.willshare.network.AwtKeyEvent.awtKeyCode;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    private DrawerLayout mainLayout;

    private KeycodeEntryPoint keycodeEntryPoint;
    private TypeLetterEntryPoint typeLetterEntryPoint;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        setContentView(R.layout.main_activity);
        mainLayout = findViewById(R.id.drawer_layout);

        keycodeEntryPoint = new KeycodeEntryPoint();
        typeLetterEntryPoint = new TypeLetterEntryPoint();

        initMenu();
        initHideSoftKeyboardOnTouchOut();
    }


    private void initMenu() {
        initToolbar();

        final NavigationView navigationView = findViewById(R.id.main_nav_view);
        navigationView.inflateHeaderView(R.layout.nav_header_main);

        try {
            MainMenu mainMenu = new MainMenu(this);
            new ConfigMenu(this, mainMenu);

        } catch (WrongConfigurationException e) {
            throw new IllegalArgumentException(e);
        }
    }


    private void initToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        if (actionbar != null) {
            actionbar.setDisplayHomeAsUpEnabled(true);
            actionbar.setHomeAsUpIndicator(R.drawable.ic_menu);
        }
    }


    private void initHideSoftKeyboardOnTouchOut() {
        mainLayout.setOnTouchListener(new HideSoftKeyboardOnTouch(this, mainLayout));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mainLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        Log.i("key pressed", String.valueOf(event.getKeyCode()));

        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            int awtKeyCode = awtKeyCode(event);
            if (awtKeyCode != 0) {
                keycodeEntryPoint.setKeyCode(awtKeyCode);
                keycodeEntryPoint.callAsync();
            } else {
                typeLetterEntryPoint.setLetter((char) event.getUnicodeChar());
                typeLetterEntryPoint.callAsync();
            }
        }
        return true;
    }

}
